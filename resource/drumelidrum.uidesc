<?xml version="1.0" encoding="UTF-8"?>
<vstgui-ui-description version="1">
    <fonts>
       <font name="parameter" font-name="Gill Sans" alternative-font-names="Gillius ADF, Nimbus Sans, Bahnschrift, Microsoft Sans Serif" size="9" bold="false" italic="false"/>
       <font name="text" font-name="Gill Sans" alternative-font-names="Gillius ADF, Nimbus Sans, Bahnschrift, Microsoft Sans Serif" size="10" bold="false" italic="false"/>
       <font name="button" font-name="Gill Sans" alternative-font-names="Gillius ADF, Nimbus Sans, Bahnschrift, Microsoft Sans Serif" size="11" bold="false" italic="false"/>
       <font name="aboutbox" font-name="Arial" size="12" bold="false" italic="false"/>
    </fonts>
    <colors>
        <color name="background" rgba="#111111ff"/>
        <color name="text-color" rgba="#808080ff"/>
        <color name="active-color" rgba="#ff7000ff"/>
        <color name="inactive-color" rgba="#333333ff"/>
        <color name="separator-color" rgba="#222222ff"/>
        <color name="text-rect-active-color" rgba="#444444ff"/>
        <color name="text-rect-inactive-color" rgba="#333333ff"/>
        <color name="text-rect-color" rgba="#808080ff"/>
        <color name="indicator-color" rgba="#ff7000ff"/>
        <color name="about-background" rgba="#000000a0"/>
        <color name="about-text-color" rgba="#f0f0f0ff"/>
    </colors>
    <custom>
    </custom>
    <variables>
        <var name="spacing" value="0.125"/>
        <var name="horizontal-spacing" value="0.0625"/>
        <var name="vertical-spacing" value="0.0625"/>
        <var name="button-radius" value="0.125"/>
        <var name="button-aspect-ratio" value="1.0"/>
        <var name="text-rect-radius" value="0.125"/>
        <var name="text-rect-aspect-ratio" value="1.0"/>
        <var name="option-menu-radius" value="0.5"/>
        <var name="option-menu-aspect-ratio" value="0.33333333"/>
        <var name="text-edit-radius" value="0.5"/>
        <var name="text-edit-aspect-ratio" value="0.33333333"/>
        <var name="switch-button-radius" value="0.125"/>
        <var name="switch-button-aspect-ratio" value="1.0"/>
        <var name="option-menu-font" value="text"/>
        <var name="switch-button-font" value="text"/>
        <var name="text-edit-font" value="text"/>
        <var name="view-switch-button-min" value="0.0"/>
        <var name="view-switch-button-max" value="23.0"/>
        <var name="parameter-spacing" value="0.0"/>
    </variables>
    <control-tags>
        <control-tag name="kPadModule" tag="0"/>
        <control-tag name="kPadVolume" tag="2"/>
        <control-tag name="kPadPan" tag="3"/>
        <control-tag name="kPadNote" tag="4"/>
        <control-tag name="kPadChoke" tag="6"/>

        <control-tag name="kBdPitch" tag="8"/>
        <control-tag name="kBdTone" tag="9"/>
        <control-tag name="kBdDecay" tag="10"/>

        <control-tag name="kSdPitch" tag="8"/>
        <control-tag name="kSdTone" tag="9"/>
        <control-tag name="kSdSnappy" tag="10"/>
        <control-tag name="kSdDecay" tag="11"/>

        <control-tag name="kHihatPitch" tag="8"/>
        <control-tag name="kHihatNoise" tag="9"/>
        <control-tag name="kHihatTone" tag="10"/>
        <control-tag name="kHihatReso" tag="11"/>
        <control-tag name="kHihatDecay" tag="12"/>

        <control-tag name="kClapTone" tag="8"/>
        <control-tag name="kClapReso" tag="9"/>
        <control-tag name="kClapDecay" tag="10"/>
        <control-tag name="kClapEarlyDecay" tag="11"/>
        <control-tag name="kClapEarlyDelay" tag="12"/>

        <control-tag name="kCymbalPitch" tag="8"/>
        <control-tag name="kCymbalTone" tag="9"/>
        <control-tag name="kCymbalReso" tag="10"/>
        <control-tag name="kCymbalEarlyDecay" tag="11"/>
        <control-tag name="kCymbalLateDecay" tag="12"/>
        <control-tag name="kCymbalThreshold" tag="13"/>
        <control-tag name="kCymbalNoise" tag="14"/>

        <control-tag name="kCbPitch" tag="8"/>
        <control-tag name="kCbDecay" tag="9"/>

        <control-tag name="kTomPitch" tag="8"/>
        <control-tag name="kTomTone" tag="9"/>
        <control-tag name="kTomDecay" tag="10"/>
        <control-tag name="kTomSweepDecay" tag="11"/>
        <control-tag name="kTomSweep" tag="12"/>
        <control-tag name="kTomSweepThreshold" tag="13"/>

        <control-tag name="kKickPitch" tag="8"/>
        <control-tag name="kKickTone" tag="9"/>
        <control-tag name="kKickDecay" tag="10"/>
        <control-tag name="kKickSweepDecay" tag="11"/>
        <control-tag name="kKickSweep" tag="12"/>
        <control-tag name="kKickSweepThreshold" tag="13"/>

        <control-tag name="kPadIndicatorBase" tag="768"/>

        <control-tag name="kViewSwitch" tag="4711"/>
    </control-tags>
    <template name="Editor" class="LayoutableContainer" layout="fill"
                       background-color="background"
                       margin="10, 10, 10, 10"
                       size="520, 200" maxSize="1040, 400" minSize="520, 200">
        <view class="AboutBox" background-color="about-background" margin="10, 10, 10, 10" layout="fill" custom-view-name="AboutBox">
            <view class="CMultiLineTextLabel" font="aboutbox" line-layout="wrap" text-alignment="center" font-color="about-text-color" transparent="true" custom-view-name="AboutBoxText"/>
        </view>
        <view class="LayoutableContainer" layout="distributed" distribution="2, 2" orientation="vertical" spacing="0.25">
        <view class="ViewSwitchContainer" template-switch-control="kViewSwitch" transparent="true" animation-style="push" animation-time="10"
           template-names="Module1,Module2,Module3,Module4,Module5,Module6,Module7,Module8,Module9,Module10,Module11,Module12,Module13,Module14,Module15,Module16,Module17,Module18,Module19,Module20,Module21,Module22,Module23,Module24" /> 
        <view class="LayoutableContainer" layout="grid" rows="3" columns="8" horizontal-spacing="0.0" vertical-spacing="0.0" >
           <view template="DrumPad" instance="0"/>
           <view template="DrumPad" instance="1"/>
           <view template="DrumPad" instance="2"/>
           <view template="DrumPad" instance="3"/>
           <view template="DrumPad" instance="4"/>
           <view template="DrumPad" instance="5"/>
           <view template="DrumPad" instance="6"/>
           <view template="DrumPad" instance="7"/>
           <view template="DrumPad" instance="8"/>
           <view template="DrumPad" instance="9"/>
           <view template="DrumPad" instance="10"/>
           <view template="DrumPad" instance="11"/>
           <view template="DrumPad" instance="12"/>
           <view template="DrumPad" instance="13"/>
           <view template="DrumPad" instance="14"/>
           <view template="DrumPad" instance="15"/>
           <view template="DrumPad" instance="16"/>
           <view template="DrumPad" instance="17"/>
           <view template="DrumPad" instance="18"/>
           <view template="DrumPad" instance="19"/>
           <view template="DrumPad" instance="20"/>
           <view template="DrumPad" instance="21"/>
           <view template="DrumPad" instance="22"/>
           <view template="DrumPad" instance="23"/>
        </view>
        </view>
    </template>
    <template name="plabel" class="LayoutableText" layout-id="label" font="parameter"/>
    <template name="DrumModuleView" class="LayoutableContainer" layout="distributed" distribution="8, 1, 8" orientation="vertical" spacing="4" instance-offset="24">
        <view class="LayoutableContainer" layout="grid" rows="1" columns="8">
            <view class="LayoutableContainer" layout="parameter" scale="0.5">
                <view class="LayoutableOptionMenu" control-tag="kPadModule"/>
            </view>
	        <view class="LayoutableContainer" layout="parameter" scale="0.5">
	            <view template="plabel" text="note"/>
	            <view class="LayoutableTextEdit" control-tag="kPadNote"/>
	        </view>
	        <view class="Placeholder"/>
	        <view class="Placeholder"/>
	        <view class="Placeholder"/>
	        <view class="Placeholder"/>
	        <view class="LayoutableContainer" layout="parameter">
	            <view template="plabel" text="volume"/>
	            <view class="LayoutableKnob" control-tag="kPadVolume"/>
	        </view>
	        <view class="LayoutableContainer" layout="parameter">
	            <view template="plabel" text="pan"/>
	            <view class="LayoutableKnob" control-tag="kPadPan" symmetric="true"/>
	        </view>
       </view>
       <view class="Layoutable" drawable="rect" color="separator-color"/>
    </template>
    <template name="BdView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
       <view class="LayoutableContainer" layout="parameter" scale="0.5">
           <view template="plabel" text="pitch"/>
           <view class="LayoutableTextEdit" control-tag="kBdPitch"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="tone"/>
           <view class="LayoutableKnob" control-tag="kBdTone"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="decay"/>
           <view class="LayoutableKnob" control-tag="kBdDecay"/>
       </view>
    </template>
    <template name="SdView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
       <view class="LayoutableContainer" layout="parameter" scale="0.5">
           <view template="plabel" text="pitch"/>
           <view class="LayoutableTextEdit" control-tag="kSdPitch"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="tone"/>
           <view class="LayoutableKnob" control-tag="kSdTone"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="snappy"/>
           <view class="LayoutableKnob" control-tag="kSdSnappy"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="decay"/>
           <view class="LayoutableKnob" control-tag="kSdDecay"/>
       </view>
    </template>
    <template name="HihatView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
       <view class="LayoutableContainer" layout="parameter" scale="0.5">
           <view template="plabel" text="pitch"/>
           <view class="LayoutableTextEdit" control-tag="kHihatPitch"/>
       </view>
       <view class="LayoutableContainer" layout="parameter" scale="0.5">
           <view template="plabel" text="noise"/>
           <view class="LayoutableOptionMenu" control-tag="kHihatNoise"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="tone"/>
           <view class="LayoutableKnob" control-tag="kHihatTone"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="reso"/>
           <view class="LayoutableKnob" control-tag="kHihatReso"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="decay"/>
           <view class="LayoutableKnob" control-tag="kHihatDecay"/>
       </view>
    </template>
    <template name="ClapView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="tone"/>
           <view class="LayoutableKnob" control-tag="kClapTone"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="reso"/>
           <view class="LayoutableKnob" control-tag="kClapReso"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="early decay"/>
           <view class="LayoutableKnob" control-tag="kClapEarlyDecy"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="early delay"/>
           <view class="LayoutableKnob" control-tag="kClapEarlyDelay"/>
       </view>
       <view class="LayoutableContainer" layout="parameter">
           <view template="plabel" text="decay"/>
           <view class="LayoutableKnob" control-tag="kClapDecay"/>
       </view>
    </template>
    <template name="CymbalView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
        <view class="LayoutableContainer" layout="parameter" scale="0.5">
            <view template="plabel" text="pitch"/>
            <view class="LayoutableTextEdit" control-tag="kCymbalPitch"/>
        </view>
        <view class="LayoutableContainer" layout="parameter" scale="0.5">
            <view template="plabel" text="noise"/>
            <view class="LayoutableOptionMenu" control-tag="kCymbalNoise"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="tone"/>
            <view class="LayoutableKnob" control-tag="kCymbalTone"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="reso"/>
            <view class="LayoutableKnob" control-tag="kCymbalReso"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="early decay"/>
            <view class="LayoutableKnob" control-tag="kCymbalEarlyDecay"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="decay"/>
            <view class="LayoutableKnob" control-tag="kCymbalLateDecay"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="threshold"/>
            <view class="LayoutableKnob" control-tag="kCymbalThreshold"/>
        </view>
    </template>
    <template name="CbView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
        <view class="LayoutableContainer" layout="parameter" scale="0.5">
            <view template="plabel" text="pitch"/>
            <view class="LayoutableTextEdit" control-tag="kCbPitch"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="decay"/>
            <view class="LayoutableKnob" control-tag="kCbDecay"/>
        </view>
    </template>
    <template name="TomView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
        <view class="LayoutableContainer" layout="parameter" scale="0.5">
            <view template="plabel" text="pitch"/>
            <view class="LayoutableTextEdit" control-tag="kTomPitch"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="tone"/>
            <view class="LayoutableKnob" control-tag="kTomTone"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="decay"/>
            <view class="LayoutableKnob" control-tag="kTomDecay"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="sweep decay"/>
            <view class="LayoutableKnob" control-tag="kTomSweepDecay"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="threshold"/>
            <view class="LayoutableKnob" control-tag="kTomSweepThreshold"/>
        </view>
    </template>
    <template name="KickView" class="LayoutableContainer" layout="grid" rows="1" columns="8">
        <view class="LayoutableContainer" layout="parameter" scale="0.5">
            <view template="plabel" text="pitch"/>
            <view class="LayoutableTextEdit" control-tag="kKickPitch"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="tone"/>
            <view class="LayoutableKnob"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="decay"/>
            <view class="LayoutableKnob" control-tag="kKickDecay"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="sweep"/>
            <view class="LayoutableKnob" control-tag="kKickSweep"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="sweep decay"/>
            <view class="LayoutableKnob" control-tag="kKickSweepDecay"/>
        </view>
        <view class="LayoutableContainer" layout="parameter">
            <view template="plabel" text="threshold"/>
            <view class="LayoutableKnob" control-tag="kKickSweepThreshold"/>
        </view>
    </template>
    <template name="Module1" class="DrumModule" instance="0"/>
    <template name="Module2" class="DrumModule" instance="1"/>
    <template name="Module3" class="DrumModule" instance="2"/>
    <template name="Module4" class="DrumModule" instance="3"/>
    <template name="Module5" class="DrumModule" instance="4"/>
    <template name="Module6" class="DrumModule" instance="5"/>
    <template name="Module7" class="DrumModule" instance="6"/>
    <template name="Module8" class="DrumModule" instance="7"/>
    <template name="Module9" class="DrumModule" instance="8"/>
    <template name="Module10" class="DrumModule" instance="9"/>
    <template name="Module11" class="DrumModule" instance="10"/>
    <template name="Module12" class="DrumModule" instance="11"/>
    <template name="Module13" class="DrumModule" instance="12"/>
    <template name="Module14" class="DrumModule" instance="13"/>
    <template name="Module15" class="DrumModule" instance="14"/>
    <template name="Module16" class="DrumModule" instance="15"/>
    <template name="Module17" class="DrumModule" instance="16"/>
    <template name="Module18" class="DrumModule" instance="17"/>
    <template name="Module19" class="DrumModule" instance="18"/>
    <template name="Module20" class="DrumModule" instance="19"/>
    <template name="Module21" class="DrumModule" instance="20"/>
    <template name="Module22" class="DrumModule" instance="21"/>
    <template name="Module23" class="DrumModule" instance="22"/>
    <template name="Module24" class="DrumModule" instance="23"/>
    <template name="DrumPad" class="LayoutableContainer" layout="layer" inset="0, 0.5">
        <view class="Layoutable" drawable="alpha-rect" control-tag="kPadIndicatorBase" color="indicator-color" instance-offset="24"/>
        <view class="ViewSwitchButton" control-tag="kViewSwitch"/>
        <view class="LayoutableText" control-tag="kPadModule" instance-offset="24" font="button"/>
    </template>
</vstgui-ui-description>
