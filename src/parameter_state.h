/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_PARAMETER_STATE_H
#define DRUMELIDRUM_PARAMETER_STATE_H

#include <filurvst/common/parameter_state.h>

namespace drumelidrum {

class ParameterState : public filurvst::State {
 public:
  ParameterState();
  virtual ~ParameterState() {}

  virtual Steinberg::tresult setState(Steinberg::IBStream* stream) override;
};

void default_parameters(ParameterState& state);

}  // namespace drumelidrum

#endif // DRUMELIDRUM_PARAMETER_STATE_H
