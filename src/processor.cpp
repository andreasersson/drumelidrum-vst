/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "processor.h"

#include "drumpad_ids.h"
#include "module_factory.h"
#include "module_parameters.h"
#include "module_types.h"
#include "parameter_ids.h"

#include <pluginterfaces/base/ustring.h>
#include <pluginterfaces/vst/ivstevents.h>
#include <pluginterfaces/vst/ivstparameterchanges.h>

#include <algorithm>
#include <cassert>
#include <cmath>

namespace drumelidrum {

Steinberg::FUID Processor::cid(0xBCEA5ADD, 0xAFD1412D, 0x827D970B, 0x528660C4);

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                                     ParameterState& state,
                                     std::vector<std::unique_ptr<module::DrumPad>>& drumpads);
static void set_parameter(Steinberg::Vst::ParamID parameter_id,
                          double value,
                          std::vector<std::unique_ptr<module::DrumPad>>& drumpads);
static void set_state_parameters(const ParameterState& state,
                                 std::vector<std::unique_ptr<module::DrumPad>>& drumpads);
static void to_sample32(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data);
static void to_sample64(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data);

Indicator::Indicator(const Steinberg::Vst::ParamID &id, double time, double silent_threshold)
    : m_id(id),
      m_silent_threshold(silent_threshold),
      m_time(time),
      m_max_num_samples(0),
      m_value(0),
      m_previous_value(0) {}

void Indicator::init(Steinberg::Vst::SampleRate sample_rate) {
  m_max_num_samples = static_cast<int>(sample_rate * m_time);
}

void Indicator::setValue(double value) {
  if (value > m_silent_threshold) {
    m_value = m_max_num_samples;
  }
}

void Indicator::process(Steinberg::Vst::IParameterChanges *parameterChanges, int num_samples) {
  if ((nullptr != parameterChanges) && (m_value != m_previous_value)) {
    Steinberg::int32 index;
    Steinberg::Vst::IParamValueQueue* queue = parameterChanges->addParameterData(m_id, index);
    if (queue) {
      double value = static_cast<double>(m_value) / m_max_num_samples;
      queue->addPoint(0, value, index);
    }
  }

  m_previous_value = m_value;

  if (m_value > num_samples) {
    m_value -= num_samples;
  } else {
    m_value = 0;
  }
}

Processor::Processor(const Steinberg::FUID& controller_id) {
  setControllerClass(controller_id);

  // Create drumpads
  for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
    std::unique_ptr<module::DrumPad> drumpad = drumpad_module(m_state, pad_id, this);
    m_drumpads.push_back(std::move(drumpad));
    m_drumpad_indicators.push_back(Indicator(pad_id * kPadOffset + kPadIndicatorBase));
  }

  // Create modules and add them to the drumpads
  for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
    int value = static_cast<int>(m_state.get(pad_base_id(pad_id)));
    module_type_t module_type = static_cast<module_type_t>(value);
    m_drumpads.at(pad_id)->setModule(module_factory(m_state, pad_id, module_type));
  }

  // Setup hihat chokes
  m_drumpads.at(kClosedHihatPad)->addChoke(kPedalHihatPad);
  m_drumpads.at(kClosedHihatPad)->addChoke(kOpenHihatPad);

  m_drumpads.at(kPedalHihatPad)->addChoke(kClosedHihatPad);
  m_drumpads.at(kPedalHihatPad)->addChoke(kOpenHihatPad);

  m_drumpads.at(kOpenHihatPad)->addChoke(kClosedHihatPad);
  m_drumpads.at(kOpenHihatPad)->addChoke(kPedalHihatPad);

  default_parameters(m_state);
}

Steinberg::tresult Processor::initialize(FUnknown* context) {
  Steinberg::tresult ret_val = AudioEffect::initialize(context);
  if (Steinberg::kResultTrue == ret_val) {
    addAudioOutput(STR16("output"), Steinberg::Vst::SpeakerArr::kStereo);
    addEventInput(STR16("event input"), 1);
  }

  return ret_val;
}

Steinberg::tresult Processor::setState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = m_state.setState(state);

  if (m_is_active) {
    set_state_parameters(m_state, m_drumpads);
  }

  return ret_val;
}

Steinberg::tresult Processor::getState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = m_state.getState(state);

  return ret_val;
}

Steinberg::tresult Processor::setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                                 Steinberg::int32 num_inputs,
                                                 Steinberg::Vst::SpeakerArrangement* outputs,
                                                 Steinberg::int32 num_outputs) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((0 == num_inputs) && (1 == num_outputs)
      && (Steinberg::Vst::SpeakerArr::kStereo == outputs[0])) {
    ret_val = AudioEffect::setBusArrangements(inputs, num_inputs, outputs, num_outputs);
  }

  return ret_val;
}

Steinberg::tresult Processor::canProcessSampleSize(Steinberg::int32 symbolic_sample_size) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((Steinberg::Vst::kSample32 == symbolic_sample_size)
      || (Steinberg::Vst::kSample64 == symbolic_sample_size)) {
    ret_val = Steinberg::kResultTrue;
  }

  return ret_val;
}

Steinberg::tresult Processor::setActive(Steinberg::TBool state) {
  m_is_active = state;
  if (state) {
    for (auto &module : m_drumpads) {
      if (module) {
        module->initialize(processSetup.sampleRate, processSetup.maxSamplesPerBlock);
      }
    }

    for (auto& indicator : m_drumpad_indicators) {
      indicator.init(processSetup.sampleRate);
    }

    m_left_buffer.resize(processSetup.maxSamplesPerBlock);
    m_right_buffer.resize(processSetup.maxSamplesPerBlock);

    set_state_parameters(m_state, m_drumpads);
  }

  return AudioEffect::setActive(state);
}

Steinberg::tresult Processor::process(Steinberg::Vst::ProcessData& data) {
  Steinberg::tresult result = Steinberg::kResultTrue;

  handle_parameter_changes(data.inputParameterChanges, m_state, m_drumpads);

  if ((data.numOutputs < 1) || (data.outputs[0].numChannels < 2)) {
    return Steinberg::kResultTrue;
  }

  Steinberg::int32 number_of_events = 0;
  Steinberg::Vst::IEventList* input_events = data.inputEvents;
  if (nullptr != input_events) {
    number_of_events = input_events->getEventCount();
  }

  assert(static_cast<size_t>(data.numSamples) <= m_left_buffer.size());
  assert(static_cast<size_t>(data.numSamples) <= m_right_buffer.size());
  double* out_left = m_left_buffer.data();
  double* out_right = m_right_buffer.data();
  memset(out_left, 0, data.numSamples * sizeof(double));
  memset(out_right, 0, data.numSamples * sizeof(double));

  bool silent = true;
  for (auto &drumpad : m_drumpads) {
    if (drumpad) {
      silent &= drumpad->silent();
    }
  }

  Steinberg::int32 number_of_frames = data.numSamples;
  Steinberg::int32 frame_offset = 0;
  Steinberg::Vst::Event event;

  for (Steinberg::int32 i = 0; i < number_of_events; i++) {
    Steinberg::tresult handle_event = input_events->getEvent(i, event);

    if (Steinberg::kResultTrue == handle_event) {
      switch (event.type) {
        // supported event types
        case Steinberg::Vst::Event::kNoteOnEvent:
          break;

        // ignore all other event types
        default:
          handle_event = Steinberg::kResultFalse;
          break;
      }
    }

    if (Steinberg::kResultTrue == handle_event) {
      Steinberg::int32 number_of_sub_frames = event.sampleOffset - frame_offset;
      assert(number_of_sub_frames >= 0);
      assert(number_of_sub_frames < data.numSamples);
      frame_offset = event.sampleOffset;
      assert(frame_offset >= 0);
      assert(frame_offset < data.numSamples);

      if (number_of_sub_frames > 0) {
        for (auto &drumpad : m_drumpads) {
          if (drumpad) {
            drumpad->process(out_left, out_right, number_of_sub_frames);
            silent &= drumpad->silent();
          }
        }

        out_left += number_of_sub_frames;
        out_right += number_of_sub_frames;
        number_of_frames -= number_of_sub_frames;
      }

      switch (event.type) {
        case Steinberg::Vst::Event::kNoteOnEvent:
          for (auto &drumpad : m_drumpads) {
            if (drumpad)
              drumpad->noteOn(event.noteOn.pitch, event.noteOn.velocity);
          }
          break;

        default:
          break;
      }
    }
  }

  if (number_of_frames > 0) {
    for (auto &drumpad : m_drumpads) {
      if (drumpad) {
        drumpad->process(out_left, out_right, number_of_frames);
        silent &= drumpad->silent();
      }
    }

  }

  if (Steinberg::Vst::kSample64 == data.symbolicSampleSize) {
    to_sample64(m_left_buffer.data(), m_right_buffer.data(), data);
  } else {
    to_sample32(m_left_buffer.data(), m_right_buffer.data(), data);
  }

  for (size_t pad_id = 0; pad_id < kNumPads; ++pad_id) {
    if (m_drumpads.at(pad_id)) {
      m_drumpad_indicators.at(pad_id).setValue(m_drumpads.at(pad_id)->level());
    }
  }

  for (auto& indicator : m_drumpad_indicators) {
    indicator.process(data.outputParameterChanges, data.numSamples);
  }

  if (silent) {
    data.outputs[0].silenceFlags = 0x03;
  }

  return result;
}

void Processor::choke(int id) {
  if ((id >= 0) && (static_cast<size_t>(id) < m_drumpads.size())) {
    m_drumpads.at(id)->choke();
  }
}

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                                     ParameterState& state,
                                     std::vector<std::unique_ptr<module::DrumPad>>& drumpads) {
  Steinberg::int32 parameter_count = 0;
  if (nullptr != parameter_changes) {
    parameter_count = parameter_changes->getParameterCount();
  }

  for (Steinberg::int32 i = 0; i < parameter_count; i++) {
    Steinberg::tresult result = Steinberg::kResultTrue;
    Steinberg::Vst::IParamValueQueue* queue = parameter_changes->getParameterData(i);
    if (nullptr == queue) {
      result = Steinberg::kResultFalse;
    }

    if ((Steinberg::kResultTrue == result) && (0 == queue->getPointCount())) {
      result = Steinberg::kResultFalse;
    }

    Steinberg::Vst::ParamValue normalized_value;
    Steinberg::int32 sample_offset;
    if (Steinberg::kResultTrue == result) {
      result = queue->getPoint(queue->getPointCount() - 1, sample_offset, normalized_value);
    }

    if (Steinberg::kResultTrue == result) {
      state.setNormalized(queue->getParameterId(), normalized_value);
      double value = state.get(queue->getParameterId());
      set_parameter(queue->getParameterId(), value, drumpads);
    }
  }
}

static void set_parameter(Steinberg::Vst::ParamID parameter_id,
                          double value,
                          std::vector<std::unique_ptr<module::DrumPad>>& drumpads) {
  for (auto &drumpad : drumpads) {
    if (drumpad) {
      drumpad->setParameter(parameter_id, value);
    }
  }
}

static void set_state_parameters(const ParameterState& state,
                                 std::vector<std::unique_ptr<module::DrumPad>>& drumpads) {
  for (auto &parameter : state.parameters()) {
    set_parameter(parameter.first, parameter.second, drumpads);
  }
}

static void to_sample32(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data) {
  for (auto n = 0; n < data.numSamples; ++n) {
    data.outputs[0].channelBuffers32[0][n] = left_buffer[n];
    data.outputs[0].channelBuffers32[1][n] = right_buffer[n];
  }
}

static void to_sample64(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data) {
  memcpy(data.outputs[0].channelBuffers64[0], left_buffer, data.numSamples * sizeof(double));
  memcpy(data.outputs[0].channelBuffers64[1], right_buffer, data.numSamples * sizeof(double));
}

}  // namespace drumelidrum
