/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_PARAMETER_ID_H
#define DRUMELIDRUM_PARAMETER_ID_H

#include <cassert>

namespace drumelidrum {

enum {
  kNumModuleParameters = 16,
  kMaxNumPads = 32,
  kNumPads = 24,
  kMaxNumPadParameters = 8,
  kNumMaxchannels = 8,
  kPadMaxNote = 127,
  kPadOffset = (kMaxNumPadParameters + kNumModuleParameters)
};

enum {
  kPadModule = 0,
  kPadChannel,
  kPadVolume,
  kPadPan,
  kPadNote,
  kPadNoteEnd,
  kPadChoke
};

enum {
  // Read only parameters
  kPadIndicatorBase = kMaxNumPads * kPadOffset
};

inline int pad_base_id(int pad_id) {
  assert(pad_id >= 0);
  assert(pad_id < kNumPads);

  return (pad_id * kPadOffset);
}

inline int module_base_id(int pad_id) {
  assert(pad_id >= 0);
  assert(pad_id < kNumPads);

  return (pad_base_id(pad_id) + kMaxNumPadParameters);
}

}  // namespaces

#endif // DRUMELIDRUM_PARAMETER_ID_H

