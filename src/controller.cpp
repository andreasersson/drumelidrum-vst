/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "controller.h"

#include "drumpad_ids.h"
#include "module_types.h"
#include "modules/modules.h"
#include "parameter_ids.h"
#include "parameter_state.h"
#include "version.h"

#include <filurvst/common/parameter.h>
#include <filurvst/gui/aboutbox.h>
#include <filurvst/gui/uiviewcreator.h>

#include <pluginterfaces/base/ustring.h>

#include <vstgui/plugin-bindings/vst3editor.h>
#include <vstgui/uidescription/iviewfactory.h>
#include <vstgui/uidescription/uiattributes.h>

#include <array>
#include <cassert>
#include <cmath>
#include <initializer_list>
#include <regex>
#include <sstream>
#include <string>

#ifdef USE_FONT_RESIZE
#include <filurvst/gui/editor.h>
using Editor = filurvst::gui::FontResizeEditor;
#else
using Editor = VSTGUI::VST3Editor;
#endif

namespace drumelidrum {

enum UnitIds {
  kPadBaseUnitId = 1,
};

Steinberg::FUID Controller::cid(0xC3E2D785, 0xA16A46A8, 0xAA95CE62, 0xB82AC2CA);

static const std::array<const char*, 12> s_note_names = { { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" } };

static VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            Steinberg::FObject* controller);
static VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                                 const VSTGUI::UIAttributes& attributes,
                                                 const VSTGUI::IUIDescription* description);

static void bd_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id);
static void sd_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id);
static void clap_parameters(Steinberg::Vst::ParameterContainer &parameters,
                            ParameterState &state,
                            int base_id,
                            Steinberg::Vst::UnitID unit_id);
static void ch_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id);
static void ph_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id);
static void oh_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id);
static void cymbal_parameters(Steinberg::Vst::ParameterContainer &parameters,
                              ParameterState &state,
                              int base_id,
                              Steinberg::Vst::UnitID unit_id);
static void tom_parameters(Steinberg::Vst::ParameterContainer &parameters,
                           ParameterState &state,
                           int base_id,
                           Steinberg::Vst::UnitID unit_id);
static void kick_parameters(Steinberg::Vst::ParameterContainer &parameters,
                            ParameterState &state,
                            int base_id,
                            Steinberg::Vst::UnitID unit_id);
static void cb_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id);

static void pitch_value_to_string(Steinberg::Vst::ParamValue value,
                                  Steinberg::Vst::String128 string);
static bool pitch_string_to_value(const Steinberg::Vst::String128 string,
                                  Steinberg::Vst::ParamValue& value);

static Steinberg::Vst::Parameter* add_pitch_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                      filurvst::State &state,
                                                      const std::string& name,
                                                      Steinberg::int32 parameter_id,
                                                      Steinberg::Vst::UnitID unit_id);
static Steinberg::Vst::Parameter* add_noise_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                      filurvst::State &state,
                                                      const std::string& name,
                                                      Steinberg::int32 parameter_id,
                                                      Steinberg::Vst::UnitID unit_id);

static void module_parameter_factory(Steinberg::Vst::ParameterContainer &parameters,
                                     ParameterState &state,
                                     module_type_t module_type,
                                     int base_id,
                                     Steinberg::Vst::UnitID unit_id);

Controller::Controller() :
    m_aboutbox(new filurvst::gui::AboutBoxController, false) {
}

Steinberg::tresult Controller::initialize(FUnknown* context) {
  Steinberg::tresult result = EditControllerEx1::initialize(context);
  if (Steinberg::kResultTrue == result) {
    ParameterState state;
    default_parameters(state);

    static const std::map<module_type_t, const char*> module_type_name = {
        { kModuleBd, "bd" },
        { kModuleSd, "sd" },
        { kModuleClap, "clap" },
        { kModuleClosedHihat, "ch" },
        { kModulePedalHihat, "ph" },
        { kModuleOpenHihat, "oh" },
        { kModuleRs, "rs" },
        { kModuleKick, "kick" },
        { kModuleTom, "tom" },
        { kModuleClaves, "claves" },
        { kModuleCowbell, "cowbell" },
        { kModuleCymbal, "cymbal" } };

    static const std::map<int32_t, const char*> pad_name_map = {
        { kAcousticBassDrumPad, "bd" },
        { kBassDrum1Pad, "bd" },
        { kAcousticSnarePad, "sd" },
        { kElectricSnarePad, "sd" },
        { kHandClapPad, "clap" },
        { kClosedHihatPad, "ch" },
        { kPedalHihatPad, "ph" },
        { kOpenHihatPad, "oh" },
        { kSideStickPad, "rs" },
        { kLowFloorTomPad, "lf tom" },
        { kHighFloorTomPad, "hf tom" },
        { kLowTomPad, "l tom" },
        { kLowMidTomPad, "lm tom" },
        { kHiMidTomPad, "hm tom" },
        { kHighTomPad, "h tom" },
        { kClavesPad, "claves" },
        { kCbPad, "cb" },
        { kCrashPad, "crash" },
        { kRidePad, "ride" },
        { kHiBongoPad, "prc 2" },
        { kLowBongoPad, "prc 1" },
        { kRideBellPad, "ride bell" },
        { kPrc3Pad, "prc 3" },
        { kClap2Pad, "clap" } };

    std::vector<std::string> module_list(kModuleNumTypes);
    for (auto &key_value : module_type_name) {
      module_list[key_value.first] = key_value.second;
    }

    // Add units
    {
      Steinberg::Vst::UnitID parent_unit_id = Steinberg::Vst::kRootUnitId;
      Steinberg::Vst::ProgramListID program_list_id = Steinberg::Vst::kNoProgramListId;

      for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
        Steinberg::Vst::UnitID unit_id = pad_id + kPadBaseUnitId;
        std::string name = pad_name_map.at(pad_id);
        addUnit(new Steinberg::Vst::Unit(Steinberg::String(name.c_str()), unit_id, parent_unit_id, program_list_id));
      }
    }

    for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
      std::string name = "pad" + std::to_string(pad_id + 1) + " ";
      int base_id = pad_id * (kMaxNumPadParameters + kNumModuleParameters);
      Steinberg::Vst::UnitID unit_id = pad_id + kPadBaseUnitId;
      Steinberg::Vst::Parameter* parameter = filurvst::add_list_parameter(parameters, state, name + "module", base_id + kPadModule, module_list, unit_id);
      // NOTE: module parameter is read-only for now.
      parameter->getInfo().flags = Steinberg::Vst::ParameterInfo::kIsReadOnly | Steinberg::Vst::ParameterInfo::kIsList;

      filurvst::add_parameter(parameters, state, name + "volume", base_id + kPadVolume, unit_id);
      filurvst::add_parameter(parameters, state, name + "pan", base_id + kPadPan, unit_id);
      add_pitch_parameter(parameters, state, name + "note", base_id + kPadNote, unit_id);
#if 0 // Not yet used
      add_range_parameter(parameters, state, name + "channel", base_id + kPadChannel, 0, kNumMaxchannels, unit_id);
      add_range_parameter(parameters, state, name + "end note", base_id + kPadNoteEnd, 0, 127, unit_id, 127);
      add_range_parameter(parameters, state, name + "choke", base_id + kPadChoke, 0, 0xffffffff, unit_id);
#endif
    }

    // Add module parameters
    for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
      int pad_base = pad_id * kPadOffset;
      int module_parameter_id = pad_base + kPadModule;
      int value = static_cast<int>(state.get(module_parameter_id));
      module_type_t module_type = static_cast<module_type_t>(value);
      int base_id = pad_base + kMaxNumPadParameters;
      Steinberg::Vst::UnitID unit_id = pad_id + kPadBaseUnitId;
      module_parameter_factory(parameters, state, module_type, base_id, unit_id);
    }

    for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
      parameters.addParameter(USTRING("-"),
                              nullptr, 0.0, 0,
                              Steinberg::Vst::ParameterInfo::kIsReadOnly,
                              pad_id * kPadOffset + kPadIndicatorBase,
                              pad_id + kPadBaseUnitId);
    }
  }

  return Steinberg::kResultTrue;
}

Steinberg::tresult Controller::terminate() {
  return EditControllerEx1::terminate();
}

Steinberg::tresult Controller::setComponentState(Steinberg::IBStream* state) {
  ParameterState parameter_state;

  Steinberg::tresult result = parameter_state.setState(state);

  for (auto &parameter : parameter_state.parameters()) {
    setParamNormalized(parameter.first, parameter_state.getNormalized(parameter.first));
  }

  return result;
}

Steinberg::tresult Controller::setParamNormalized(Steinberg::Vst::ParamID tag,
                                                  Steinberg::Vst::ParamValue value) {
  return EditControllerEx1::setParamNormalized(tag, value);
}

Steinberg::tresult Controller::openAboutBox(Steinberg::TBool only_check) {
  Steinberg::tresult ret_val = Steinberg::kResultTrue;
  if (!only_check && m_aboutbox) {
    m_aboutbox->open();
  }

  return ret_val;
}

Steinberg::IPlugView* Controller::createView(Steinberg::FIDString _name) {
  Steinberg::ConstString name(_name);
  Steinberg::IPlugView *plug_view = nullptr;

  filurvst::gui::ui_view_creator_init();
  if (Steinberg::Vst::ViewType::kEditor == name) {
    plug_view = new Editor(this, "Editor", "drumelidrum.uidesc");
  }

  return plug_view;
}

VSTGUI::CView* Controller::createCustomView(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            VSTGUI::VST3Editor* /*editor*/) {
  assert(nullptr != description);

  VSTGUI::CView* view = custom_view_about_box(name, attributes, description, m_aboutbox.get());

  if (nullptr == view) {
    view = custom_view_about_box_text(name, attributes, description);
  }

  return view;
}

VSTGUI::COptionMenu* Controller::createContextMenu(const VSTGUI::CPoint& /*pos*/, VSTGUI::VST3Editor* /*editor*/) {
  VSTGUI::COptionMenu* menu = nullptr;

  if (m_aboutbox) {
    std::unique_ptr<VSTGUI::COptionMenu> option_menu(new VSTGUI::COptionMenu());
    std::unique_ptr<VSTGUI::CCommandMenuItem> item(new VSTGUI::CCommandMenuItem(VSTGUI::CCommandMenuItem::Desc("About")));
    item->setActions([aboutbox = m_aboutbox.get()](VSTGUI::CCommandMenuItem*){ aboutbox->open(); });
    option_menu->addEntry(item.release());

    menu = option_menu.release();
  }

  return menu;
}

VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description,
                                     Steinberg::FObject* controller) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBox") == 0)) {
    view = factory->createView(attributes, description);
    filurvst::gui::AboutBox* aboutbox = dynamic_cast<filurvst::gui::AboutBox*>(view);
    if (nullptr != aboutbox) {
      aboutbox->setController(controller);
    }
  }

  return view;
}

VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBoxText") == 0)) {
    std::string about_text;
    about_text += stringPluginName "\n";
    about_text += "version: " FULL_VERSION_STR "\n";
    about_text += stringCompanyWeb"\n\n";
    about_text += stringLegalCopyright"\n";

    about_text += "This program comes with ABSOLUTELY NO WARRANTY.\n";
    about_text += "This is free software, and you are welcome to redistribute it under certain conditions.\n";
    about_text += "You should have received a copy of the GNU General Public License along with " stringPluginName ".\n";
    about_text += "If not, see <https://www.gnu.org/licenses/>.\n";

    VSTGUI::UIAttributes local_attributes = attributes;
    local_attributes.setAttribute("title", about_text);
    view = factory->createView(local_attributes, description);
  }

  return view;
}

static void bd_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "bd pitch", base_id + module::BdParameters::kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "bd tone", base_id + module::BdParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "bd decay", base_id + module::BdParameters::kDecay, unit_id);
}

static void sd_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "sd pitch", base_id + module::SdParameters::kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "sd tone", base_id + module::SdParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "sd snappy", base_id + module::SdParameters::kSnappy, unit_id);
  filurvst::add_parameter(parameters, state, "sd decay", base_id + module::SdParameters::kDecay, unit_id);
}

static void clap_parameters(Steinberg::Vst::ParameterContainer &parameters,
                            ParameterState &state,
                            int base_id,
                            Steinberg::Vst::UnitID unit_id) {
  filurvst::add_parameter(parameters, state, "clap tone", base_id + module::ClapParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "clap resonance", base_id + module::ClapParameters::kReso, unit_id);
  filurvst::add_parameter(parameters, state, "clap early decay", base_id + module::ClapParameters::kEarlyDecay, unit_id);
  filurvst::add_parameter(parameters, state, "clap early delay", base_id + module::ClapParameters::kEarlyDelay, unit_id);
  filurvst::add_parameter(parameters, state, "clap decay", base_id + module::ClapParameters::kDecay, unit_id);
}

static void ch_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "closed hihat pitch", base_id + module::HihatParameters::kPitch, unit_id);
  add_noise_parameter(parameters, state, "closed hihat noise", base_id + module::HihatParameters::kNoise, unit_id);
  filurvst::add_parameter(parameters, state, "closed hihat tone", base_id + module::HihatParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "closed hihat resonance", base_id + module::HihatParameters::kReso, unit_id);
  filurvst::add_parameter(parameters, state, "closed hihat decay", base_id + module::HihatParameters::kClosedDecay, unit_id);
}

static void ph_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "pedal hihat pitch", base_id + module::HihatParameters::kPitch, unit_id);
  add_noise_parameter(parameters, state, "pedal hihat noise", base_id + module::HihatParameters::kNoise, unit_id);
  filurvst::add_parameter(parameters, state, "pedal hihat tone", base_id + module::HihatParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "pedal hihat resonance", base_id + module::HihatParameters::kReso, unit_id);
  filurvst::add_parameter(parameters, state, "pedal hihat decay", base_id + module::HihatParameters::kClosedDecay, unit_id);
}

static void oh_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "open hihat pitch", base_id + module::HihatParameters::kPitch, unit_id);
  add_noise_parameter(parameters, state, "open hihat noise", base_id + module::HihatParameters::kNoise, unit_id);
  filurvst::add_parameter(parameters, state, "open hihat tone", base_id + module::HihatParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "open hihat resonance", base_id + module::HihatParameters::kReso, unit_id);
  filurvst::add_parameter(parameters, state, "open hihat decay", base_id + module::HihatParameters::kClosedDecay, unit_id);
}

static void cymbal_parameters(Steinberg::Vst::ParameterContainer &parameters,
                              ParameterState &state,
                              int base_id,
                              Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "cymbal pitch", base_id + module::CymbalParameters::kPitch, unit_id);
  add_noise_parameter(parameters, state, "cymbal noise", base_id + module::CymbalParameters::kNoise, unit_id);
  filurvst::add_parameter(parameters, state, "cymbal tone", base_id + module::CymbalParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "cymbal resonance", base_id + module::CymbalParameters::kReso, unit_id);
  filurvst::add_parameter(parameters, state, "cymbal early decay", base_id + module::CymbalParameters::kEarlyDecay, unit_id);
  filurvst::add_parameter(parameters, state, "cymbal decay threshold", base_id + module::CymbalParameters::kDecayThreshold, unit_id);
  filurvst::add_parameter(parameters, state, "cymbal late decay", base_id + module::CymbalParameters::kLateDecay, unit_id);
}

static void tom_parameters(Steinberg::Vst::ParameterContainer &parameters,
                           ParameterState &state,
                           int base_id,
                           Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "tom pitch", base_id + module::TomParameters::kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "tom tone", base_id + module::TomParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "tom decay", base_id + module::TomParameters::kDecay, unit_id);
  filurvst::add_parameter(parameters, state, "tom sweep decay", base_id + module::TomParameters::kSweepDecay, unit_id);
  filurvst::add_parameter(parameters, state, "tom sweep threshold", base_id + module::TomParameters::kSweepThreshold, unit_id);
}

static void kick_parameters(Steinberg::Vst::ParameterContainer &parameters,
                            ParameterState &state,
                            int base_id,
                            Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "kick pitch", base_id + module::BdParameters::kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "kick tone", base_id + module::BdParameters::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "kick decay", base_id + module::BdParameters::kDecay, unit_id);
  filurvst::add_parameter(parameters, state, "kick sweep", base_id + module::BdParameters::kSweep, unit_id);
  filurvst::add_parameter(parameters, state, "kick sweep decay", base_id + module::BdParameters::kSweepDecay, unit_id);
  filurvst::add_parameter(parameters, state, "kick sweep threshold", base_id + module::BdParameters::kSweepThreshold, unit_id);
}

static void cb_parameters(Steinberg::Vst::ParameterContainer &parameters,
                          ParameterState &state,
                          int base_id,
                          Steinberg::Vst::UnitID unit_id) {
  add_pitch_parameter(parameters, state, "cb pitch", base_id + module::CbParameters::kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "cb decay", base_id + module::CbParameters::kDecay, unit_id);
}

static void module_parameter_factory(Steinberg::Vst::ParameterContainer &parameters,
                                     ParameterState &state,
                                     module_type_t module_type,
                                     int base_id,
                                     Steinberg::Vst::UnitID unit_id) {
  switch (module_type) {
    case kModuleBd:
      bd_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleSd:
    case kModuleRs:
    case kModuleClaves:
      sd_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleClosedHihat:
      ch_parameters(parameters, state, base_id, unit_id);
      break;

    case kModulePedalHihat:
      ph_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleOpenHihat:
      oh_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleCymbal:
      cymbal_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleClap:
      clap_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleCowbell:
      cb_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleTom:
      tom_parameters(parameters, state, base_id, unit_id);
      break;

    case kModuleKick:
      kick_parameters(parameters, state, base_id, unit_id);
      break;

    default:
      assert(false);
      break;
  }
}

static void pitch_value_to_string(Steinberg::Vst::ParamValue value,
                                  Steinberg::Vst::String128 string) {
  size_t octave = value / s_note_names.size();
  size_t note = std::min(s_note_names.size() - 1, static_cast<size_t>(value - octave * s_note_names.size()));

  std::stringstream stream;
  stream << s_note_names[note] << octave;

  Steinberg::UString wrapper(string, str16BufferSize(Steinberg::Vst::String128));
  wrapper.fromAscii(stream.str().c_str());
}

static bool pitch_string_to_value(const Steinberg::Vst::String128 string,
                                  Steinberg::Vst::ParamValue& value) {
  bool ret_val = false;
  char c_str[32];
  Steinberg::UString wrapper(const_cast<Steinberg::Vst::TChar*>(string),
                             str16BufferSize(Steinberg::Vst::String128));
  wrapper.toAscii(c_str, sizeof(c_str));

  std::cmatch match;
  std::regex pitch_regex("([a-gA-G]#?)(\\d)");
  std::regex_match(c_str, match, pitch_regex);

  std::string note_name;
  size_t note_number = 0;
  int octave = 0;
  if (match.size() == 3) {
    ret_val = true;
    note_name = match[1].str();
    std::transform(note_name.begin(), note_name.end(), note_name.begin(), ::toupper);
    note_number = std::distance(s_note_names.begin(), std::find(s_note_names.begin(), s_note_names.end(), note_name));
    if (note_number >= s_note_names.size()) {
      ret_val = false;
    }
    try {
      octave = std::stol(match[2].str());
    } catch (...) {
      ret_val = false;
    }
  }
  if (ret_val) {
    value = note_number + 12 * octave;
  }

  if (!ret_val) {
    ret_val = true;
    try {
      value = std::min(std::stod(c_str), 127.0);
      value = std::floor(std::max(value, 0.0));
    } catch (...) {
      ret_val = false;
    }
  }

  return ret_val;
}

static Steinberg::Vst::Parameter* add_pitch_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                      filurvst::State &state,
                                                      const std::string& name,
                                                      Steinberg::int32 parameter_id,
                                                      Steinberg::Vst::UnitID unit_id) {
  return filurvst::add_range_parameter(parameters,
                                        state,
                                        name,
                                        parameter_id,
                                        0,
                                        kPadMaxNote,
                                        unit_id,
                                        kPadMaxNote,
                                        pitch_value_to_string,
                                        pitch_string_to_value);
}
static Steinberg::Vst::Parameter* add_noise_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                      filurvst::State &state,
                                                      const std::string& name,
                                                      Steinberg::int32 parameter_id,
                                                      Steinberg::Vst::UnitID unit_id) {
  std::initializer_list<std::string> list = { "noise1", "noise2", "noise3", "noise4" };
  return filurvst::add_list_parameter(parameters, state, name, parameter_id, list, unit_id);
}

}  // namespace drumelidrum
