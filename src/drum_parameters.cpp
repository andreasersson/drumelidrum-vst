/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "drum_parameters.h"

#include <filur/drums/cymbal.h>

#include <filurvst/common/parameter.h>

#include <pluginterfaces/base/ustring.h>

#include <array>
#include <cmath>
#include <initializer_list>
#include <regex>
#include <sstream>
#include <string>

namespace drumelidrum {

static const std::array<const char*, 12> s_note_names = { { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" } };

static void pitch_value_to_string(Steinberg::Vst::ParamValue value, Steinberg::Vst::String128 string) {
  size_t octave = value / s_note_names.size();
  size_t note = std::min(s_note_names.size() - 1, static_cast<size_t>(value - octave * s_note_names.size()));

  std::stringstream stream;
  stream << s_note_names[note] << octave;

  Steinberg::UString wrapper(string, str16BufferSize(Steinberg::Vst::String128));
  wrapper.fromAscii(stream.str().c_str());
}

static bool pitch_string_to_value(const Steinberg::Vst::String128 string, Steinberg::Vst::ParamValue& value) {
  bool ret_val = false;
  char c_str[32];
  Steinberg::UString wrapper(const_cast<Steinberg::Vst::TChar*>(string), str16BufferSize(Steinberg::Vst::String128));
  wrapper.toAscii(c_str, sizeof(c_str));

  std::cmatch match;
  std::regex pitch_regex("([a-gA-G]#?)(\\d)");
  std::regex_match(c_str, match, pitch_regex);

  std::string note_name;
  size_t note_number = 0;
  int octave = 0;
  if (match.size() == 3) {
    ret_val = true;
    note_name = match[1].str();
    std::transform(note_name.begin(), note_name.end(), note_name.begin(), ::toupper);
    note_number = std::distance(s_note_names.begin(), std::find(s_note_names.begin(), s_note_names.end(), note_name));
    if (note_number >= s_note_names.size()) {
      ret_val = false;
    }
    try {
      octave = std::stol(match[2].str());
    } catch (...) {
      ret_val = false;
    }
  }
  if (ret_val) {
    value = note_number + 12 * octave;
  }

  if (!ret_val) {
    ret_val = true;
    try {
      value = std::min(std::stod(c_str), 127.0);
      value = std::floor(std::max(value, 0.0));
    } catch (...) {
      ret_val = false;
    }
  }

  return ret_val;
}

static Steinberg::Vst::Parameter* add_pitch_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                      filurvst::State &state,
                                                      const std::string& name,
                                                      Steinberg::int32 parameter_id,
                                                      Steinberg::Vst::UnitID unit_id) {
  return filurvst::add_range_parameter(parameters,
                                       state,
                                       name,
                                       parameter_id,
                                       0,
                                       127,
                                       unit_id,
                                       127,
                                       pitch_value_to_string,
                                       pitch_string_to_value);
}

static Steinberg::Vst::Parameter* add_noise_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                      filurvst::State &state,
                                                      const std::string& name,
                                                      Steinberg::int32 parameter_id,
                                                      Steinberg::Vst::UnitID unit_id) {
  std::initializer_list<std::string> list = { "noise1", "noise2", "noise3", "noise4" };
  return filurvst::add_list_parameter(parameters, state, name, parameter_id, list, unit_id);
}

namespace bd {

void init_state(filurvst::State& state)
{
  state.addInt(kLowNote, 0, 0, 127);
  state.addInt(kHighNote, 127, 0, 127);
  state.addInt(kPitch, 0, 0, 127);
  state.add(kTone, 0.75);
  state.addSqr(kDecay, 1.5, 50e-3, 2000e-3);
  state.addSqr(kSweepDecay, 500e-3, 50e-3, 2000e-3);
  state.addSqr(kSweep, 1.0, 0.0, 1.0);
  state.add(kSweepThreshold, 0.055);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "low note", kLowNote, unit_id);
  add_pitch_parameter(parameters, state, "high note", kHighNote, unit_id);
  add_pitch_parameter(parameters, state, "pitch", kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "tone", kTone, unit_id);
  filurvst::add_parameter(parameters, state, "decay", kDecay, unit_id);
}

} // namespace bd

namespace tom {

void init_state(filurvst::State& state)
{
  state.addInt(bd::kLowNote, 0, 0, 127);
  state.addInt(bd::kHighNote, 127, 0, 127);
  state.addInt(bd::kPitch, 0, 0, 127);
  state.add(bd::kTone, 0.25);
  state.addSqr(bd::kDecay, 750e-3, 50e-3, 2000e-3);
  state.addSqr(bd::kSweepDecay, 500e-3, 50e-3, 2000e-3);
  state.addSqr(bd::kSweep, 1.0, 0.0, 2.0);
  state.add(bd::kSweepThreshold, 0.055);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "low note", bd::kLowNote, unit_id);
  add_pitch_parameter(parameters, state, "high note", bd::kHighNote, unit_id);
  add_pitch_parameter(parameters, state, "pitch", bd::kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "tone", bd::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "decay", bd::kDecay, unit_id);
  filurvst::add_parameter(parameters, state, "sweep decay", bd::kSweepDecay, unit_id);
  filurvst::add_parameter(parameters, state, "sweep threshold", bd::kSweepThreshold, unit_id);
}

} // namespace tom

namespace kick {

void init_state(filurvst::State& state)
{
  state.addInt(bd::kLowNote, 0, 0, 127);
  state.addInt(bd::kHighNote, 127, 0, 127);
  state.addInt(bd::kPitch, 0, 0, 127);
  state.add(bd::kTone, 0.5);
  state.addSqr(bd::kDecay, 500e-3, 50e-3, 2000e-3);
  state.addSqr(bd::kSweepDecay, 100e-3, 50e-3, 2000e-3);
  state.addSqr(bd::kSweep, 2.0, 0.0, 2.0);
  state.add(bd::kSweepThreshold, 1.0);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "low note", bd::kLowNote, unit_id);
  add_pitch_parameter(parameters, state, "high note", bd::kHighNote, unit_id);
  add_pitch_parameter(parameters, state, "pitch", bd::kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "tone", bd::kTone, unit_id);
  filurvst::add_parameter(parameters, state, "decay", bd::kDecay, unit_id);
  filurvst::add_parameter(parameters, state, "sweep", bd::kSweep, unit_id);
  filurvst::add_parameter(parameters, state, "sweep decay", bd::kSweepDecay, unit_id);
  filurvst::add_parameter(parameters, state, "sweep threshold", bd::kSweepThreshold, unit_id);
}

} // namespace kick

namespace sd {

void init_state(filurvst::State& state)
{
  state.addInt(kLowNote, 0, 0, 127);
  state.addInt(kHighNote, 127, 0, 127);
  state.addInt(kPitch, 0, 0, 127);
  state.add(kTone, 0.75);
  state.add(kSnappy, 0.55);
  state.addSqr(kDecay, 200e-3, 1e-3, 1000e-3);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "low note", kLowNote, unit_id);
  add_pitch_parameter(parameters, state, "high note", kHighNote, unit_id);
  add_pitch_parameter(parameters, state, "pitch", kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "tone", kTone, unit_id);
  filurvst::add_parameter(parameters, state, "snappy", kSnappy, unit_id);
  filurvst::add_parameter(parameters, state, "decay", kDecay, unit_id);
}

} // namespace sd

namespace clap {

void init_state(filurvst::State& state)
{
  state.addInt(kLowNote, 0, 0, 127);
  state.addInt(kHighNote, 127, 0, 127);
  state.add(kTone, 0.4);
  state.add(kReso, 0.6);
  state.addSqr(kDecay, 240e-3, 20e-3, 1000e-3);
  state.addSqr(kEarlyDecay, 50e-3, 20e-3, 200e-3);
  state.addSqr(kEarlyDelay, 15e-3, 5e-3, 30e-3);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "low note", kLowNote, unit_id);
  add_pitch_parameter(parameters, state, "high note", kHighNote, unit_id);
  filurvst::add_parameter(parameters, state, "tone", kTone, unit_id);
  filurvst::add_parameter(parameters, state, "resonance", kReso, unit_id);
  filurvst::add_parameter(parameters, state, "decay", kDecay, unit_id);
  filurvst::add_parameter(parameters, state, "early decay", kEarlyDecay, unit_id);
  filurvst::add_parameter(parameters, state, "early delay", kEarlyDelay, unit_id);
}

} // namespace clap

namespace cymbal {

void init_state(filurvst::State& state)
{
  state.addInt(kLowNote, 0, 0, 127);
  state.addInt(kHighNote, 127, 0, 127);
  state.addInt(kPitch, 0, 0, 127);
  state.add(kTone, 0.5);
  state.add(kReso, 0.33);
  state.addSqr(kEarlyDecay, 0.07, 50e-3, 400e-3);
  state.addSqr(kLateDecay, 3.0, 200e-3, 4000e-3);
  state.add(kDecayThreshold, 0.3);
  state.addInt(kNoise, METAL_NOISE_TYPE_4, 0, METAL_NOISE_NUM_TYPES - 1);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "low note", kLowNote, unit_id);
  add_pitch_parameter(parameters, state, "high note", kHighNote, unit_id);
  add_pitch_parameter(parameters, state, "pitch", kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "tone", kTone, unit_id);
  filurvst::add_parameter(parameters, state, "resonance", kReso, unit_id);
  filurvst::add_parameter(parameters, state, "early decay", kEarlyDecay, unit_id);
  filurvst::add_parameter(parameters, state, "late decay", kLateDecay, unit_id);
  filurvst::add_parameter(parameters, state, "decay threshold", kDecayThreshold, unit_id);
  add_noise_parameter(parameters, state, "noise", kNoise, unit_id);
}

} // namespace cymbal

namespace hihat {

void init_state(filurvst::State& state)
{
  state.addInt(kClosedNote, 42, 0, 127);
  state.addInt(kOpenNote, 46, 0, 127);
  state.addInt(kPedalNote, 44, 0, 127);
  state.addInt(kPitch, 36, 0, 127);
  state.addInt(kNoise, METAL_NOISE_TYPE_1, 0, METAL_NOISE_NUM_TYPES - 1);
  state.add(kTone, 0.4);
  state.add(kReso, 0.5);
  state.addSqr(kClosedDecay, 90e-3, 50e-3, 500e-3);
  state.addSqr(kOpenDecay, 2.0, 200e-3, 4000e-3);
  state.addSqr(kPedalDecay, 400e-3, 200e-3, 2000e-3);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "closed note", kClosedNote, unit_id);
  add_pitch_parameter(parameters, state, "open note", kOpenNote, unit_id);
  add_pitch_parameter(parameters, state, "pedal note", kPedalNote, unit_id);
  add_pitch_parameter(parameters, state, "pitch", kPitch, unit_id);
  add_noise_parameter(parameters, state, "noise", kNoise, unit_id);
  filurvst::add_parameter(parameters, state, "tone", kTone, unit_id);
  filurvst::add_parameter(parameters, state, "resonance", kReso, unit_id);
  filurvst::add_parameter(parameters, state, "early decay", kClosedDecay, unit_id);
  filurvst::add_parameter(parameters, state, "late decay", kOpenDecay, unit_id);
  filurvst::add_parameter(parameters, state, "decay threshold", kPedalDecay, unit_id);
}

} // namespace hihat

namespace cb {

void init_state(filurvst::State& state)
{
  state.addInt(kLowNote, 0, 0, 127);
  state.addInt(kHighNote, 127, 0, 127);
  state.addInt(kPitch, 0, 0, 127);
  state.addSqr(kDecay, 200e-3, 50e-3, 2000e-3);
}

void init_parameters(Steinberg::Vst::ParameterContainer& parameters)
{
  filurvst::State state;
  init_state(state);
  const Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId;

  add_pitch_parameter(parameters, state, "low note", kLowNote, unit_id);
  add_pitch_parameter(parameters, state, "high note", kHighNote, unit_id);
  add_pitch_parameter(parameters, state, "pitch", kPitch, unit_id);
  filurvst::add_parameter(parameters, state, "decay", kDecay, unit_id);
}

} // namespace cb

} // namespace drumelidrum
