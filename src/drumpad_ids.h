/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUM_PAD_ID_H
#define DRUM_PAD_ID_H

namespace drumelidrum {

enum {
  kAcousticBassDrumPad = 0,
  kBassDrum1Pad,
  kAcousticSnarePad,
  kElectricSnarePad,
  kSideStickPad,
  kClosedHihatPad,
  kPedalHihatPad,
  kOpenHihatPad,
  kHandClapPad,
  kClap2Pad,
  kCbPad,
  kClavesPad,
  kPrc3Pad,
  kCrashPad,
  kRidePad,
  kRideBellPad,
  kHiBongoPad,
  kLowBongoPad,
  kLowFloorTomPad,
  kHighFloorTomPad,
  kLowTomPad,
  kLowMidTomPad,
  kHiMidTomPad,
  kHighTomPad
};

}  // namespace drumelidrum

#endif // DRUM_PAD_ID_H
