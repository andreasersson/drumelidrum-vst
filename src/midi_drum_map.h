/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MIDI_DRUM_MAP_H
#define MIDI_DRUM_MAP_H

enum MidiDrumMap {
  kAcousticBassDrum = 35,
  kBassDrum1 = 36,
  kSideStick = 37,
  kAcousticSnare = 38,
  kHandClap = 39,
  kElectricSnare = 40,
  kLowFloorTom = 41,
  kClosedHihat = 42,
  kHighFloorTom = 43,
  kPedalHihat = 44,
  kLowTom = 45,
  kOpenHihat = 46,
  kLowMidTom = 47,
  kHiMidTom = 48,
  kCrashCymbal1 = 49,
  kHighTom = 50,
  kRideCymbal1 = 51,
  kChineseCymbal = 52,
  kRideBell = 53,
  kTambourine = 54,
  kSplashCymbal = 55,
  kCowbell = 56,
  kRideCymbal2 = 57,
  kHiBongo = 60,
  kLowBongo = 61,
  kMuteHiConga = 62,
  kOpenHiConga = 63,
  kLowConga = 64,
  kHighTimbale = 65,
  kLowTimbale = 66,
  kHighAgogo = 67,
  kLowAgogo = 68,
  kCabasa = 69,
  kMaracas = 70,
  kShortWhistle = 71,
  kLongWhistle = 72,
  kShortGuiro = 73,
  kLongGuiro = 74,
  kClaves = 75,
  kHiWoodBlock = 76,
  kLowWoodBlock = 77,
  kMuteCuica = 78,
  kOpenCuica = 79,
  kMuteTriangle = 80,
  kOpenTriangle = 81
};

#endif // MIDI_DRUM_MAP_H
