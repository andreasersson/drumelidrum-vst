/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_MODULE_TYPES_H
#define DRUMELIDRUM_MODULE_TYPES_H

namespace drumelidrum {

// NOTE: Don't change the order or remove any entries in this enum. Only add new at the end.
typedef enum {
  kModuleBd = 0,
  kModuleSd,
  kModuleClosedHihat,
  kModulePedalHihat,
  kModuleOpenHihat,
  kModuleCymbal,
  kModuleClap,
  kModuleRs,
  kModuleClaves,
  kModuleCowbell,
  kModuleTom,
  kModuleKick,
  // NOTE: Add new modules before kModuleNumTypes
  kModuleNumTypes
} module_type_t;

}  // namespace drumelidrum

#endif // DRUMELIDRUM_MODULE_TYPES_H
