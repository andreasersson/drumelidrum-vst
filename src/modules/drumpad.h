/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_DRUM_PAD_H
#define DRUMELIDRUM_DRUM_PAD_H

#include "../module.h"

#include <filur/mixer.h>

#include <filurvst/common/parameter_state.h>

#include <memory>
#include <unordered_set>

namespace module {

class DrumPadParameters : public ParameterIf {
 public:
  enum {
    kModule,
    kChannel,
    kVolume,
    kPan,
    kStartNote,
    kEndNote,
    kChoke
  };

  DrumPadParameters(filurvst::State &parameter_state,
                    int drumpad_id,
                    const parameter_map_t &parameter_ids);
  virtual ~DrumPadParameters() {}

  virtual void initialize(double sample_rate) override;
  virtual void handleParameter(int32_t parameter_id, double value) override;

  mixer_parameters_t mixer = {};
  int note_nr;
  int id;
};

class DrumPad : public ModuleIf {
 public:
  explicit DrumPad(std::unique_ptr<DrumPadParameters> parameters,
                   ChokeIf *choke = nullptr)
      :
      m_parameters(std::move(parameters)),
      m_choke(choke) {}
  DrumPad(std::unique_ptr<ModuleIf> module,
          std::unique_ptr<DrumPadParameters> parameters,
          ChokeIf* choke = nullptr)
      : m_module(std::move(module)),
        m_parameters(std::move(parameters)),
        m_choke(choke) {}
  virtual ~DrumPad() {}

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int note_nr) override;
  virtual void choke() override {
    if (m_module) {
      m_module->choke();
    }
  }
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override {
    return (m_module ? m_module->silent() : true);
  }
  virtual double level() const override {
    return (m_module ? m_module->level() : 0.0);
  }

  virtual void setParameter(int32_t parameter_id, double value) override {
    if (m_parameters) {
      m_parameters->set(parameter_id, value);
    }
    if (m_module) {
      m_module->setParameter(parameter_id, value);
    }
  }
  virtual ParameterIf* getParameters() override {
    return (m_module) ? m_module->getParameters() : nullptr;
  }

  void setModule(std::unique_ptr<ModuleIf> module) {
    m_module = std::move(module);
  }

  void addChoke(int id) {
    m_choke_ids.emplace(id);
  }
  void removeChoke(int id) {
    m_choke_ids.erase(id);
  }

 protected:
  std::unique_ptr<ModuleIf> m_module;
  std::unique_ptr<DrumPadParameters> m_parameters;
  mixer_t m_mixer = {};
  std::vector<double> m_left_buffer;
  std::vector<double> m_right_buffer;
  ChokeIf* m_choke;
  std::unordered_set<int> m_choke_ids;
};

}  // namespace module

#endif // DRUMELIDRUM_DRUM_PAD_H
