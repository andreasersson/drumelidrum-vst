/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sd_module.h"

#include "pitch_to_frequency.h"

#include <cassert>

namespace module {

static const PitchToFrequency s_pitch;

SdParameters::SdParameters(filurvst::State &parameter_state,
                           const parameter_map_t &parameter_ids)
    : ParameterIf(parameter_ids) {
  const double default_pitch = 48;
  const double default_tone = 0.75;
  const double default_snappy = 0.55;
  const double default_decay = 200e-3;

  updateParameters(parameter_state, parameter_ids);
  setValues(parameter_state,
            m_parameter_ids,
            default_pitch,
            default_tone,
            default_snappy,
            default_decay);
}

void SdParameters::initialize(double sample_rate) {
  sd_init(sample_rate, &sd);
}

void SdParameters::handleParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kPitch:
      sd_set_frequency(s_pitch.frequency(value), &sd);
      break;

    case kTone:
      sd_set_tone(value, &sd);
      break;

    case kSnappy:
      sd_set_snappy(value, &sd);
      break;

    case kDecay:
      sd_set_decay(value, &sd);
      break;

    default:
      assert(false);
      break;
  };
}

void SdParameters::updateParameters(filurvst::State &parameter_state,
                                    const parameter_map_t &parameter_ids) {
  static const double min_decay = 50e-3;
  static const double max_decay = 1000e-3;
  updateParameters(parameter_state, parameter_ids, min_decay, max_decay);
}

void SdParameters::updateParameters(filurvst::State &parameter_state,
                                    const parameter_map_t &parameter_ids,
                                    double min_decay,
                                    double max_decay) {
  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.updateUInt(key_value.first, 0.0, 127.0);
        break;

      case kTone:
        parameter_state.update(key_value.first);
        break;

      case kSnappy:
        parameter_state.update(key_value.first);
        break;

      case kDecay:
        parameter_state.updateSqr(key_value.first, min_decay, max_decay);
        break;

      default:
        break;
    }
  }
}

void SdParameters::setValues(filurvst::State &parameter_state,
                             const parameter_map_t &parameter_ids,
                             double pitch,
                             double tone,
                             double snappy,
                             double decay) {
  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.set(key_value.first, pitch);
        break;

      case kTone:
        parameter_state.set(key_value.first, tone);
        break;

      case kSnappy:
        parameter_state.set(key_value.first, snappy);
        break;

      case kDecay:
        parameter_state.set(key_value.first, decay);
        break;

      default:
        break;
    }
  }
}

void Sd::initialize(double sample_rate, size_t /*max_block_size*/) {
  if (m_parameters) {
    m_parameters->initialize(sample_rate);
  }
}

void Sd::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    sd_set_gain(velocity, &m_parameters->sd);
    sd_note_on(&m_parameters->sd);
  }
}

void Sd::choke() {}

void Sd::process(double *left, double *right, int number_of_frames) {
  if (m_parameters) {
    sd_process(left, number_of_frames, &m_parameters->sd);
    memcpy(right, left, number_of_frames * sizeof(double));
  }
}

}  // namespace module
