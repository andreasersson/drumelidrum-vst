/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_BD_MODULE_H
#define DRUMELIDRUM_BD_MODULE_H

#include "../module.h"

#include <filur/drums/bd.h>

#include <filurvst/common/parameter_state.h>

#include <memory>

namespace module {

class BdParameters : public ParameterIf {
 public:
  enum {
    kPitch,
    kTone,
    kDecay,
    kSweepDecay,
    kSweep,
    kSweepThreshold
  };

  BdParameters(filurvst::State &parameter_state, const parameter_map_t &parameter_ids);
  virtual ~BdParameters() {}

  virtual void initialize(double sample_rate) override;
  virtual void handleParameter(int32_t parameter_id, double value) override;

  static void updateParameters(filurvst::State &parameter_state,
                               const parameter_map_t &parameter_ids);
  static void updateParameters(filurvst::State &parameter_state,
                               const parameter_map_t &parameter_ids,
                               double min_decay,
                               double max_decay,
                               double min_sweep_decay,
                               double max_sweep_decay,
                               double min_sweep,
                               double max_sweep);
  static void setDefaultValues(filurvst::State &parameter_state,
                               const parameter_map_t &parameter_ids,
                               double tone,
                               double decay,
                               double sweep_decay,
                               double sweep,
                               double sweep_threshold);

  bd_parameters_t bd = {};
};

class Bd : public ModuleIf {
 public:
  explicit Bd(std::unique_ptr<BdParameters> parameters)
      : m_parameters(std::move(parameters)) {
  }
  virtual ~Bd() {}

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int /*note_nr*/) override {}
  virtual void choke() override;
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override { return (BD_SILENT_TRUE == m_bd.silent); }
  virtual double level() const override { return m_bd.low_decay_envelope.value; }

  virtual void setParameter(int32_t parameter_id, double value) override {
    if (m_parameters) {
      m_parameters->set(parameter_id, value);
    }
  }
  virtual ParameterIf* getParameters() override { return m_parameters.get(); }

 protected:
  std::unique_ptr<BdParameters> m_parameters;
  bd_t m_bd = {};
};

}  // namespace module

#endif // DRUMELIDRUM_BD_MODULE_H

