/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "clap_module.h"

#include <cassert>

namespace module {

ClapParameters::ClapParameters(filurvst::State &parameter_state,
                               const parameter_map_t &parameter_ids)
    : ParameterIf(parameter_ids) {
  updateParameters(parameter_state, parameter_ids);

  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kTone:
        parameter_state.set(key_value.first, 0.4);
        break;

      case kReso:
        parameter_state.set(key_value.first, 0.6);
        break;

      case kDecay:
        parameter_state.set(key_value.first, 240e-3);
        break;

      case kEarlyDecay:
        parameter_state.set(key_value.first, 50e-3);
        break;

      case kEarlyDelay:
        parameter_state.set(key_value.first, 15e-3);
        break;

      default:
        assert(false);
        break;
    };
  }

}

void ClapParameters::initialize(double /*sample_rate*/) {
  clap_init_parameters(&clap);
}

void ClapParameters::handleParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kTone:
      clap_set_cutoff(value, &clap);
      break;

    case kReso:
      clap_set_resonance(value, &clap);
      break;

    case kDecay:
      clap_set_decay(value, &clap);
      break;

    case kEarlyDecay:
      clap_set_early_decay(value, &clap);
      break;

    case kEarlyDelay:
      clap_set_early_delay(value, &clap);
      break;

    default:
      assert(false);
      break;
  };
}

void ClapParameters::updateParameters(filurvst::State &parameter_state,
                                      const parameter_map_t &parameter_ids) {
  static const double min_decay = 20e-3;
  static const double max_decay = 1000e-3;
  static const double min_early_decay = 20e-3;
  static const double max_early_decay = 200e-3;
  static const double min_early_delay = 5e-3;
  static const double max_early_delay = 30e-3;

  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kTone:
        parameter_state.update(key_value.first);
        break;

      case kReso:
        parameter_state.update(key_value.first);
        break;

      case kDecay:
        parameter_state.updateSqr(key_value.first, min_decay, max_decay);
        break;

      case kEarlyDecay:
        parameter_state.updateSqr(key_value.first, min_early_decay,
                                  max_early_decay);
        break;

      case kEarlyDelay:
        parameter_state.update(key_value.first, min_early_delay,
                               max_early_delay);
        break;

      default:
        assert(false);
        break;
    };
  }
}

void Clap::initialize(double sample_rate, size_t /*max_block_size*/) {
  if (m_parameters) {
    m_parameters->initialize(sample_rate);
  }
  clap_init(sample_rate, &m_clap);
}

void Clap::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    clap_set_gain(velocity, &m_parameters->clap);
    clap_note_on(&m_parameters->clap, &m_clap);
  }
}

void Clap::choke() {}

void Clap::process(double *left, double *right, int number_of_frames) {
  if (m_parameters) {
    clap_process(left, number_of_frames, &m_parameters->clap, &m_clap);
    memcpy(right, left, number_of_frames * sizeof(double));
  }
}

}  // namespace module
