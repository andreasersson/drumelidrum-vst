/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "claves_module.h"

namespace module {

ClavesParameters::ClavesParameters(filurvst::State &parameter_state,
                                   const parameter_map_t &parameter_ids)
    : module::SdParameters(parameter_state, parameter_ids) {
  const double default_pitch = 84;
  const double default_tone = 0.35;
  const double default_snappy = 0.0;
  const double default_decay = 20e-3;

  updateParameters(parameter_state, parameter_ids);
  SdParameters::setValues(parameter_state,
                          m_parameter_ids,
                          default_pitch,
                          default_tone,
                          default_snappy,
                          default_decay);
}
void ClavesParameters::updateParameters(filurvst::State &parameter_state,
                                        const parameter_map_t &parameter_ids) {
  const double min_decay = 20e-3;
  const double max_decay = 100e-3;
  SdParameters::updateParameters(parameter_state, parameter_ids, min_decay, max_decay);
}

}  // namespace module
