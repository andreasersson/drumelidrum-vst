/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "tom_module.h"

namespace module {

TomParameters::TomParameters(filurvst::State &parameter_state,
                             const parameter_map_t &parameter_ids)
    : BdParameters(parameter_state, parameter_ids) {
  const double default_tone = 0.25;
  const double default_decay = 750e-3;
  const double default_sweep_decay = 500e-3;
  const double default_sweep = 1.0;
  const double default_sweep_threshold = 0.055;

  updateParameters(parameter_state, parameter_ids);
  BdParameters::setDefaultValues(parameter_state,
                                 parameter_ids,
                                 default_tone,
                                 default_decay,
                                 default_sweep_decay,
                                 default_sweep,
                                 default_sweep_threshold);
}

void TomParameters::updateParameters(filurvst::State &parameter_state,
                                     const parameter_map_t &parameter_ids) {
  static const double bd_min_decay = 50e-3;
  static const double bd_max_decay = 2000e-3;
  static const double bd_min_sweep = 0.0;
  static const double bd_max_sweep = 2.0;

  BdParameters::updateParameters(parameter_state,
                                 parameter_ids,
                                 bd_min_decay,
                                 bd_max_decay,
                                 bd_min_decay,
                                 bd_max_decay,
                                 bd_min_sweep,
                                 bd_max_sweep);
}

void Tom::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    bd_set_gain(velocity, &m_parameters->bd);
    bd_note_on_tom(&m_parameters->bd, &m_bd);
  }
}

}  // namespace module

