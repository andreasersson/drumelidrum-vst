/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cb_module.h"

#include "pitch_to_frequency.h"

#include <cassert>

namespace module {

static const PitchToFrequency s_pitch;

CbParameters::CbParameters(filurvst::State &parameter_state, const parameter_map_t &parameter_ids)
    : ParameterIf(parameter_ids) {
  updateParameters(parameter_state, parameter_ids);

  // Set default values
  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.set(key_value.first, 60);
        break;

      case kDecay:
        parameter_state.set(key_value.first, 200e-3);
        break;

      default:
        assert(false);
        break;
    };
  }
}

void CbParameters::initialize(double sample_rate) {
  cb_init(sample_rate, &cb);
}

void CbParameters::handleParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kPitch:
      cb_set_frequency(s_pitch.frequency(value), &cb);
      break;

    case kDecay:
      cb_set_decay(value, &cb);
      break;

    default:
      assert(false);
      break;
  };
}

void CbParameters::updateParameters(filurvst::State &parameter_state,
                                    const parameter_map_t &parameter_ids) {
  static const double min_decay = 80e-3;
  static const double max_decay = 1000e-3;

  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.updateUInt(key_value.first, 0, 127.0);
        break;

      case kDecay:
        parameter_state.updateSqr(key_value.first, min_decay, max_decay);
        break;

      default:
        assert(false);
        break;
    };
  }
}

void Cb::initialize(double sample_rate, size_t /*max_block_size*/) {
  if (m_parameters) {
    m_parameters->initialize(sample_rate);
  }
}

void Cb::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    cb_set_gain(velocity, &m_parameters->cb);
    cb_note_on(&m_parameters->cb);
  }
}

void Cb::choke() {}

void Cb::process(double *left, double *right, int number_of_frames) {
  if (m_parameters) {
    cb_process(left, number_of_frames, &m_parameters->cb);
    memcpy(right, left, number_of_frames * sizeof(double));
  }
}

}  // namespace module
