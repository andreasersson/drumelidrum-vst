/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hihat_module.h"

#include "pitch_to_frequency.h"

#include <cassert>

namespace module {

static const PitchToFrequency s_pitch;

HihatParameters::HihatParameters(filurvst::State &parameter_state,
                                 const parameter_map_t &parameter_ids)
    : ParameterIf(parameter_ids) {

  updateParameters(parameter_state, parameter_ids);

  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.set(key_value.first, 36);
        break;

      case kNoise:
        parameter_state.set(key_value.first, METAL_NOISE_TYPE_1);
        break;

      case kTone:
        parameter_state.set(key_value.first, 0.4);
        break;

      case kReso:
        parameter_state.set(key_value.first, 0.5);
        break;

      case kClosedDecay:
        parameter_state.set(key_value.first, 0.09);
        break;

      case kOpenDecay:
        parameter_state.set(key_value.first, 2.0);
        break;

      case kPedalDecay:
        parameter_state.set(key_value.first, 0.40);
        break;

      default:
        assert(false);
        break;
    };
  }
}

void HihatParameters::initialize(double /*sample_rate*/) {
  hihat_init_parameters(&hihat);
}

void HihatParameters::handleParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kPitch:
      hihat_set_frequency(s_pitch.frequency(value), &hihat);
      break;

    case kNoise:
      hihat_set_noise(metal_noise_type(value), &hihat);
      break;

    case kTone:
      hihat_set_cutoff(value, &hihat);
      break;

    case kReso:
      hihat_set_resonance(value, &hihat);
      break;

    case kClosedDecay:
      hihat_set_closed_decay(value, &hihat);
      break;

    case kOpenDecay:
      hihat_set_open_decay(value, &hihat);
      break;

    case kPedalDecay:
      hihat_set_pedal_decay(value, &hihat);
      break;

    default:
      assert(false);
      break;
  };
}

void HihatParameters::updateParameters(filurvst::State &parameter_state,
                                       const parameter_map_t &parameter_ids) {
  static const double closed_hihat_min_decay = 50e-3;
  static const double closed_hihat_max_decay = 500e-3;
  static const double open_hihat_min_decay = 200e-3;
  static const double open_hihat_max_decay = 4000e-3;
  static const double pedal_hihat_min_decay = 200e-3;
  static const double pedal_hihat_max_decay = 2000e-3;

  static const double min_noise_type = METAL_NOISE_TYPE_1;
  static const double max_noise_type = METAL_NOISE_NUM_TYPES - 1;

  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.updateUInt(key_value.first, 0, 127.0);
        break;

      case kNoise:
        parameter_state.updateInt(key_value.first, min_noise_type,
                                  max_noise_type);
        break;

      case kTone:
        parameter_state.update(key_value.first);
        break;

      case kReso:
        parameter_state.update(key_value.first);
        break;

      case kClosedDecay:
        parameter_state.updateSqr(key_value.first, closed_hihat_min_decay,
                                  closed_hihat_max_decay);
        break;

      case kOpenDecay:
        parameter_state.updateSqr(key_value.first, open_hihat_min_decay,
                                  open_hihat_max_decay);
        break;

      case kPedalDecay:
        parameter_state.updateSqr(key_value.first, pedal_hihat_min_decay,
                                  pedal_hihat_max_decay);
        break;

      default:
        assert(false);
        break;
    };
  }
}

void OpenHihat::initialize(double sample_rate, size_t /*max_block_size*/) {
  if (m_parameters) {
    m_parameters->initialize(sample_rate);
  }
  hihat_init(sample_rate, &m_hihat);
}

void OpenHihat::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    hihat_set_gain(velocity, &m_parameters->hihat);
    hihat_open_on(&m_parameters->hihat, &m_hihat);
  }
}

void OpenHihat::choke() {
  if (m_parameters) {
    hihat_choke(&m_parameters->hihat, &m_hihat);
  }
}

void OpenHihat::process(double *left, double *right, int number_of_frames) {
  if (m_parameters) {
    hihat_process(left, number_of_frames, &m_parameters->hihat, &m_hihat);
    memcpy(right, left, number_of_frames * sizeof(double));
  }
}

void ClosedHihat::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    hihat_set_gain(velocity, &m_parameters->hihat);
    hihat_closed_on(&m_parameters->hihat, &m_hihat);
  }
}

void PedalHihat::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    hihat_set_gain(velocity, &m_parameters->hihat);
    hihat_pedal_on(&m_parameters->hihat, &m_hihat);
  }
}

}  // namespace module
