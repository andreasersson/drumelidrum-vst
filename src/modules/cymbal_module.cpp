/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cymbal_module.h"

#include "pitch_to_frequency.h"

#include <cassert>

namespace module {

static const PitchToFrequency s_pitch;

CymbalParameters::CymbalParameters(filurvst::State &parameter_state,
                                   const parameter_map_t &parameter_ids)
    : ParameterIf(parameter_ids) {
  updateParameters(parameter_state, parameter_ids);

  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.set(key_value.first, 48);
        break;

      case kTone:
        parameter_state.set(key_value.first, 0.5);
        break;

      case kReso:
        parameter_state.set(key_value.first, 0.33);
        break;

      case kEarlyDecay:
        parameter_state.set(key_value.first, 0.07);
        break;

      case kLateDecay:
        parameter_state.set(key_value.first, 3.00);
        break;

      case kDecayThreshold:
        parameter_state.set(key_value.first, 0.30);
        break;

      case kNoise:
        parameter_state.set(key_value.first, METAL_NOISE_TYPE_4);
        break;

      default:
        assert(false);
        break;
    };
  }
}

void CymbalParameters::initialize(double /*sample_rate*/) {
  cymbal_init_parameters(&cymbal);
}

void CymbalParameters::handleParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kPitch:
      cymbal_set_frequency(s_pitch.frequency(value), &cymbal);
      break;

    case kTone:
      cymbal_set_cutoff(value, &cymbal);
      break;

    case kReso:
      cymbal_set_resonance(value, &cymbal);
      break;

    case kEarlyDecay:
      cymbal_set_early_decay(value, &cymbal);
      break;

    case kLateDecay:
      cymbal_set_late_decay(value, &cymbal);
      break;

    case kDecayThreshold:
      cymbal_set_decay_threshold(value, &cymbal);
      break;

    case kNoise:
      cymbal_set_noise(metal_noise_type(value), &cymbal);
      break;

    default:
      assert(false);
      break;
  };
}

void CymbalParameters::updateParameters(filurvst::State &parameter_state,
                                        const parameter_map_t &parameter_ids) {
  static const double s_cymbal_min_early_decay = 50e-3;
  static const double s_cymbal_max_early_decay = 400e-3;
  static const double s_cymbal_min_late_decay = 200e-3;
  static const double s_cymbal_max_late_decay = 4000e-3;

  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.updateUInt(key_value.first, 0, 127.0);
        break;

      case kTone:
        parameter_state.update(key_value.first);
        break;

      case kReso:
        parameter_state.update(key_value.first);
        break;

      case kEarlyDecay:
        parameter_state.updateSqr(key_value.first, s_cymbal_min_early_decay,
                                  s_cymbal_max_early_decay);
        break;

      case kLateDecay:
        parameter_state.updateSqr(key_value.first, s_cymbal_min_late_decay,
                                  s_cymbal_max_late_decay);
        break;

      case kDecayThreshold:
        parameter_state.update(key_value.first);
        break;

      case kNoise:
        parameter_state.updateUInt(key_value.first, 0,
                                   METAL_NOISE_NUM_TYPES - 1);
        break;

      default:
        assert(false);
        break;
    };
  }
}

void Cymbal::initialize(double sample_rate, size_t /*max_block_size*/) {
  if (m_parameters) {
    m_parameters->initialize(sample_rate);
  }
  cymbal_init(sample_rate, &m_cymbal);
}

void Cymbal::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    cymbal_set_gain(velocity, &m_parameters->cymbal);
    cymbal_note_on(&m_parameters->cymbal, &m_cymbal);
  }
}

void Cymbal::choke() {
  if (m_parameters) {
    cymbal_choke(&m_parameters->cymbal, &m_cymbal);
  }
}

void Cymbal::process(double *left, double *right, int number_of_frames) {
  if (m_parameters) {
    cymbal_process(left, number_of_frames, &m_parameters->cymbal, &m_cymbal);
    memcpy(right, left, number_of_frames * sizeof(double));
  }
}

}  // namespace module
