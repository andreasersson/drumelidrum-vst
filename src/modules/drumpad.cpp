/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "drumpad.h"

#include "../drumpad_ids.h"
#include "../midi_drum_map.h"

#include <cassert>

namespace module {

DrumPadParameters::DrumPadParameters(filurvst::State& /*parameter_state*/,
                                     int drumpad_id,
                                     const parameter_map_t &parameter_ids)
    : ParameterIf(parameter_ids),
      note_nr(-1),
      id(drumpad_id) {}

void DrumPadParameters::initialize(double /*sample_rate*/) {
  mixer_init_parameters(&mixer);
}

void DrumPadParameters::handleParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kModule:
      break;

    case kChannel:
      break;

    case kVolume:
      mixer_set_volume(value, &mixer);
      break;

    case kPan:
      mixer_set_pan(value, &mixer);
      break;

    case kStartNote:
      note_nr = value;
      break;

    case kEndNote:
      break;

    case kChoke:
      break;

    default:
      assert(false);
      break;
  };
}

void DrumPad::initialize(double sample_rate, size_t max_block_size) {
  m_left_buffer.resize(max_block_size);
  m_right_buffer.resize(max_block_size);

  mixer_init(&m_mixer);

  if (m_parameters) {
    m_parameters->initialize(sample_rate);
  }
  if (m_module) {
    m_module->initialize(sample_rate, max_block_size);
  }
}

void DrumPad::noteOn(int note_nr, double velocity) {
  if ((m_module) && (m_parameters) && (m_parameters->note_nr == note_nr)) {
    m_module->noteOn(note_nr, velocity);
    if (nullptr != m_choke) {
      for (auto &id : m_choke_ids) {
        m_choke->choke(id);
      }
    }
  }
}

void DrumPad::noteOff(int note_nr) {
  if ((m_module) && (m_parameters) && (m_parameters->note_nr == note_nr)) {
    m_module->noteOff(note_nr);
  }
}

void DrumPad::process(double *left, double *right, int number_of_frames) {
  if ((m_module) && (m_parameters)) {
    if (m_module->silent()) {
      mixer_update_parameters(&m_parameters->mixer, &m_mixer);
    } else {
      assert(number_of_frames <= static_cast<int>(m_left_buffer.size()));
      assert(number_of_frames <= static_cast<int>(m_right_buffer.size()));

      m_module->process(m_left_buffer.data(), m_right_buffer.data(), number_of_frames);
      mixer_process_stereo(m_left_buffer.data(),
                           m_right_buffer.data(),
                           left,
                           right,
                           number_of_frames,
                           &m_parameters->mixer,
                           &m_mixer);
    }
  }
}

}  // namespace module

