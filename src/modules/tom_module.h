/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_TOM_MODULE_H
#define DRUMELIDRUM_TOM_MODULE_H

#include "bd_module.h"

#include <filurvst/common/parameter_state.h>

namespace module {

class TomParameters : public BdParameters {
 public:
  TomParameters(filurvst::State &parameter_state, const parameter_map_t &parameter_ids);
  virtual ~TomParameters() {}

  static void updateParameters(filurvst::State &parameter_state,
                               const parameter_map_t &parameter_ids);
};

class Tom : public Bd {
 public:
  explicit Tom(std::unique_ptr<BdParameters> parameters)
      : Bd(std::move(parameters)) {}
  virtual ~Tom() {}

  virtual void noteOn(int note_nr, double velocity) override;
};

}  // namespace module

#endif // DRUMELIDRUM_TOM_MODULE_H
