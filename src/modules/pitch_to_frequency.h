/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_PITCH_TO_FREQUENCY_H
#define DRUMELIDRUM_PITCH_TO_FREQUENCY_H

#include <vector>

namespace module {

class PitchToFrequency {
 public:
  PitchToFrequency();
  double frequency(int pitch) const;

 private:
  std::vector<double> m_table;
};

}  // namespace module

#endif // DRUMELIDRUM_PITCH_TO_FREQUENCY_H
