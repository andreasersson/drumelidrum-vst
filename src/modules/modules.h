/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_MODULES_H
#define DRUMELIDRUM_MODULES_H

#include "bd_module.h"
#include "cb_module.h"
#include "clap_module.h"
#include "claves_module.h"
#include "cymbal_module.h"
#include "drumpad.h"
#include "hihat_module.h"
#include "kick_module.h"
#include "rs_module.h"
#include "sd_module.h"
#include "tom_module.h"

#endif // DRUMELIDRUM_MODULES_H
