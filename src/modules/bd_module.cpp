/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "bd_module.h"

#include "pitch_to_frequency.h"

#include <cassert>

namespace module {

static const PitchToFrequency s_pitch;

BdParameters::BdParameters(filurvst::State &parameter_state, const parameter_map_t &parameter_ids)
    : ParameterIf(parameter_ids) {
  const double default_tone = 0.25;
  const double default_decay = 400e-3;
  const double default_sweep_decay = 500e-3;
  const double default_sweep = 1.0;
  const double default_sweep_threshold = 0.055;

  updateParameters(parameter_state, parameter_ids);
  setDefaultValues(parameter_state,
                   parameter_ids,
                   default_tone,
                   default_decay,
                   default_sweep_decay,
                   default_sweep,
                   default_sweep_threshold);
}

void BdParameters::initialize(double sample_rate) {
  (void) sample_rate;

  bd_init_parameters(&bd);
}

void BdParameters::handleParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kPitch:
      bd_set_frequency(s_pitch.frequency(value), &bd);
      break;

    case kTone:
      bd_set_tone(value, &bd);
      break;

    case kDecay:
      bd_set_decay(value, &bd);
      break;

    case kSweepDecay:
      bd_set_sweep_decay(value, &bd);
      break;

    case kSweep:
      bd_set_sweep(value, &bd);
      break;

    case kSweepThreshold:
      bd_set_sweep_threshold(value, &bd);
      break;

    default:
      assert(false);
      break;
  };
}

void BdParameters::updateParameters(filurvst::State &parameter_state,
                                    const parameter_map_t &parameter_ids) {
  static const double bd_min_decay = 50e-3;
  static const double bd_max_decay = 2000e-3;
  static const double bd_min_sweep = 0.0;
  static const double bd_max_sweep = 1.0;

  updateParameters(parameter_state,
                   parameter_ids,
                   bd_min_decay,
                   bd_max_decay,
                   bd_min_decay,
                   bd_max_decay,
                   bd_min_sweep,
                   bd_max_sweep);
}

void BdParameters::updateParameters(filurvst::State &parameter_state,
                                    const parameter_map_t &parameter_ids,
                                    double min_decay,
                                    double max_decay,
                                    double min_sweep_decay,
                                    double max_sweep_decay,
                                    double min_sweep,
                                    double max_sweep) {
  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.updateUInt(key_value.first, 0.0, 127.0);
        break;

      case kTone:
        parameter_state.update(key_value.first);
        break;

      case kDecay:
        parameter_state.updateSqr(key_value.first, min_decay, max_decay);
        break;

      case kSweepDecay:
        parameter_state.updateSqr(key_value.first, min_sweep_decay,
                                  max_sweep_decay);
        break;

      case kSweep:
        parameter_state.updateSqr(key_value.first, min_sweep, max_sweep);
        break;

      case kSweepThreshold:
        parameter_state.update(key_value.first);
        break;

      default:
        break;
    }
  }

}

void BdParameters::setDefaultValues(filurvst::State &parameter_state,
                                    const parameter_map_t &parameter_ids,
                                    double tone,
                                    double decay,
                                    double sweep_decay,
                                    double sweep,
                                    double sweep_threshold) {
  for (auto &key_value : parameter_ids) {
    switch (key_value.second) {
      case kPitch:
        parameter_state.set(key_value.first, 29);
        break;

      case kTone:
        parameter_state.set(key_value.first, tone);
        break;

      case kDecay:
        parameter_state.set(key_value.first, decay);
        break;

      case kSweepDecay:
        parameter_state.set(key_value.first, sweep_decay);
        break;

      case kSweep:
        parameter_state.set(key_value.first, sweep);
        break;

      case kSweepThreshold:
        parameter_state.set(key_value.first, sweep_threshold);
        break;

      default:
        break;
    }
  }
}

void Bd::initialize(double sample_rate, size_t /*max_block_size*/) {
  if (m_parameters) {
    m_parameters->initialize(sample_rate);
  }
  bd_init(sample_rate, &m_bd);
}

void Bd::noteOn(int /*note_nr*/, double velocity) {
  if (m_parameters) {
    bd_set_gain(velocity, &m_parameters->bd);
    bd_note_on_classic(&m_parameters->bd, &m_bd);
  }
}

void Bd::choke() {}

void Bd::process(double *left, double *right, int number_of_frames) {
  if (m_parameters) {
    bd_process(left, number_of_frames, &m_parameters->bd, &m_bd);
    memcpy(right, left, number_of_frames * sizeof(double));
  }
}

}  // namespace module
