/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_MODULE_PARAMETERS_H
#define DRUMELIDRUM_MODULE_PARAMETERS_H

#include "parameter_ids.h"
#include "modules/modules.h"

#include <cassert>

namespace drumelidrum {

inline module::ParameterIf::parameter_map_t drumpad_parameter_map(int pad_id) {
  const int base_id = pad_base_id(pad_id);
  module::ParameterIf::parameter_map_t pmap({
    { base_id + kPadModule, module::DrumPadParameters::kModule },
    { base_id + kPadChannel, module::DrumPadParameters::kChannel },
    { base_id + kPadVolume, module::DrumPadParameters::kVolume },
    { base_id + kPadPan, module::DrumPadParameters::kPan },
    { base_id + kPadNote, module::DrumPadParameters::kStartNote },
    { base_id + kPadChoke, module::DrumPadParameters::kChoke } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t bd_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::BdParameters::kPitch, module::BdParameters::kPitch },
    { base_id + module::BdParameters::kTone, module::BdParameters::kTone },
    { base_id + module::BdParameters::kDecay, module::BdParameters::kDecay } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t kick_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::BdParameters::kPitch, module::BdParameters::kPitch },
    { base_id + module::BdParameters::kTone, module::BdParameters::kTone },
    { base_id + module::BdParameters::kDecay, module::BdParameters::kDecay },
    { base_id + module::BdParameters::kSweepDecay, module::BdParameters::kSweepDecay },
    { base_id + module::BdParameters::kSweep, module::BdParameters::kSweep },
    { base_id + module::BdParameters::kSweepThreshold, module::BdParameters::kSweepThreshold } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t tom_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::BdParameters::kPitch, module::BdParameters::kPitch },
    { base_id + module::BdParameters::kTone, module::BdParameters::kTone },
    { base_id + module::BdParameters::kDecay, module::BdParameters::kDecay },
    { base_id + module::BdParameters::kSweepDecay, module::BdParameters::kSweepDecay },
    { base_id + module::BdParameters::kSweepThreshold, module::BdParameters::kSweepThreshold } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t sd_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::SdParameters::kPitch, module::SdParameters::kPitch },
    { base_id + module::SdParameters::kTone, module::SdParameters::kTone },
    { base_id + module::SdParameters::kSnappy, module::SdParameters::kSnappy },
    { base_id + module::SdParameters::kDecay, module::SdParameters::kDecay } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t clap_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::ClapParameters::kTone, module::ClapParameters::kTone },
    { base_id + module::ClapParameters::kReso, module::ClapParameters::kReso },
    { base_id + module::ClapParameters::kDecay, module::ClapParameters::kDecay },
    { base_id + module::ClapParameters::kEarlyDecay, module::ClapParameters::kEarlyDecay },
    { base_id + module::ClapParameters::kEarlyDelay, module::ClapParameters::kEarlyDelay } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t closed_hihat_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::HihatParameters::kPitch, module::HihatParameters::kPitch },
    { base_id + module::HihatParameters::kNoise, module::HihatParameters::kNoise },
    { base_id + module::HihatParameters::kTone, module::HihatParameters::kTone },
    { base_id + module::HihatParameters::kReso, module::HihatParameters::kReso },
    { base_id + module::HihatParameters::kClosedDecay, module::HihatParameters::kClosedDecay } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t pedal_hihat_parameter_map(
    int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::HihatParameters::kPitch, module::HihatParameters::kPitch },
    { base_id + module::HihatParameters::kNoise, module::HihatParameters::kNoise },
    { base_id + module::HihatParameters::kTone, module::HihatParameters::kTone },
    { base_id + module::HihatParameters::kReso, module::HihatParameters::kReso },
    { base_id + module::HihatParameters::kClosedDecay, module::HihatParameters::kPedalDecay } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t open_hihat_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::HihatParameters::kPitch,module::HihatParameters::kPitch },
    { base_id + module::HihatParameters::kNoise, module::HihatParameters::kNoise },
    { base_id + module::HihatParameters::kTone, module::HihatParameters::kTone },
    { base_id + module::HihatParameters::kReso, module::HihatParameters::kReso },
    { base_id + module::HihatParameters::kClosedDecay, module::HihatParameters::kOpenDecay } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t cb_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::CbParameters::kPitch, module::CbParameters::kPitch },
    { base_id + module::CbParameters::kDecay, module::CbParameters::kDecay } });

  return pmap;
}

inline module::ParameterIf::parameter_map_t cymbal_parameter_map(int pad_id) {
  const int base_id = module_base_id(pad_id);

  module::ParameterIf::parameter_map_t pmap({
    { base_id + module::CymbalParameters::kPitch, module::CymbalParameters::kPitch },
    { base_id + module::CymbalParameters::kTone, module::CymbalParameters::kTone },
    { base_id + module::CymbalParameters::kReso, module::CymbalParameters::kReso },
    { base_id + module::CymbalParameters::kEarlyDecay, module::CymbalParameters::kEarlyDecay },
    { base_id + module::CymbalParameters::kLateDecay, module::CymbalParameters::kLateDecay },
    { base_id + module::CymbalParameters::kDecayThreshold, module::CymbalParameters::kDecayThreshold },
    { base_id + module::CymbalParameters::kNoise, module::CymbalParameters::kNoise } });

  return pmap;
}

}  // namespace drumelidrum

#endif // DRUMELIDRUM_MODULE_PARAMETERS_H
