/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "drum_controller.h"

#include "drum_parameters.h"
#include "version.h"

#include <filurvst/common/parameter_state.h>
#include <filurvst/gui/aboutbox.h>
#include <filurvst/gui/uiviewcreator.h>

#include <vstgui/plugin-bindings/vst3editor.h>
#include <vstgui/uidescription/iviewfactory.h>
#include <vstgui/uidescription/uiattributes.h>

#include <memory>

#ifdef USE_FONT_RESIZE
#include <filurvst/gui/editor.h>
using Editor = filurvst::gui::FontResizeEditor;
#else
using Editor = VSTGUI::VST3Editor;
#endif

namespace drumelidrum {

static VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            Steinberg::FObject* controller);
static VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                                 const VSTGUI::UIAttributes& attributes,
                                                 const VSTGUI::IUIDescription* description);

Steinberg::tresult DrumController::initialize(FUnknown* context) {
  Steinberg::tresult result = EditControllerEx1::initialize(context);
  if ((Steinberg::kResultTrue == result) && m_initialize) {
    m_initialize(parameters);
  }

  return Steinberg::kResultTrue;
}

Steinberg::tresult DrumController::setComponentState(Steinberg::IBStream* stream) {
  Steinberg::tresult result = Steinberg::kResultFalse;
  if (m_state) {
    result = m_state->setState(stream);
    for (auto &parameter : m_state->parameters()) {
      setParamNormalized(parameter.first, m_state->getNormalized(parameter.first));
    }
  }

  return result;
}

Steinberg::IPlugView* DrumController::createView(Steinberg::FIDString _name) {
  std::string name(_name);
  Steinberg::IPlugView *plug_view = nullptr;

  filurvst::gui::ui_view_creator_init();
  if (Steinberg::Vst::ViewType::kEditor == name) {
    plug_view = new Editor(this, m_template_name.c_str(), m_uidesc.c_str());
  }

  return plug_view;
}

Steinberg::tresult DrumController::openAboutBox(Steinberg::TBool only_check) {
  Steinberg::tresult ret_val = Steinberg::kResultTrue;
  if (!only_check && m_aboutbox) {
    m_aboutbox->open();
  }

  return ret_val;
}

VSTGUI::CView* DrumController::createCustomView(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            VSTGUI::VST3Editor* /*editor*/) {
  assert(nullptr != description);

  VSTGUI::CView* view = custom_view_about_box(name, attributes, description, m_aboutbox.get());

  if (nullptr == view) {
    view = custom_view_about_box_text(name, attributes, description);
  }

  return view;
}

VSTGUI::COptionMenu* DrumController::createContextMenu(const VSTGUI::CPoint& /*pos*/, VSTGUI::VST3Editor* /*editor*/) {
  VSTGUI::COptionMenu* menu = nullptr;

  if (m_aboutbox) {
    std::unique_ptr<VSTGUI::COptionMenu> option_menu(new VSTGUI::COptionMenu());
    std::unique_ptr<VSTGUI::CCommandMenuItem> item(new VSTGUI::CCommandMenuItem(VSTGUI::CCommandMenuItem::Desc("About")));
    item->setActions([aboutbox = m_aboutbox.get()](VSTGUI::CCommandMenuItem*){ aboutbox->open(); });
    option_menu->addEntry(item.release());

    menu = option_menu.release();
  }

  return menu;
}

VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description,
                                     Steinberg::FObject* controller) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBox") == 0)) {
    view = factory->createView(attributes, description);
    filurvst::gui::AboutBox* aboutbox = dynamic_cast<filurvst::gui::AboutBox*>(view);
    if (nullptr != aboutbox) {
      aboutbox->setController(controller);
    }
  }

  return view;
}

VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBoxText") == 0)) {
    std::string about_text;
    about_text += stringPluginName " version: " FULL_VERSION_STR "\n";
    about_text += stringCompanyWeb"\n";
    about_text += stringLegalCopyright"\n";
    about_text += "license: <https://www.gnu.org/licenses/>\n";

    VSTGUI::UIAttributes local_attributes = attributes;
    local_attributes.setAttribute("title", about_text);
    view = factory->createView(local_attributes, description);
  }

  return view;
}

Steinberg::FUnknown* createBdController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  bd::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;
  controller.reset(new DrumController("bd", "drums.uidesc", std::move(state), bd::init_parameters));
  return controller.release();
}

Steinberg::FUnknown* createTomController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  tom::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;
  controller.reset(new DrumController("tom", "drums.uidesc", std::move(state), tom::init_parameters));
  return controller.release();
}

Steinberg::FUnknown* createKickController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  kick::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;
  controller.reset(new DrumController("kick", "drums.uidesc", std::move(state), kick::init_parameters));
  return controller.release();
}

Steinberg::FUnknown* createSdController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  sd::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;
  controller.reset(new DrumController("sd", "drums.uidesc", std::move(state), sd::init_parameters));
  return controller.release();
}

Steinberg::FUnknown* createClapController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  clap::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;
  controller.reset(new DrumController("clap", "drums.uidesc", std::move(state), clap::init_parameters));
  return controller.release();
}

Steinberg::FUnknown* createCymbalController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  cymbal::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;
  controller.reset(new DrumController("cymbal", "drums.uidesc", std::move(state), cymbal::init_parameters));
  return controller.release();
}

Steinberg::FUnknown* createHihatController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  hihat::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;
  controller.reset(new DrumController("hihat", "drums.uidesc", std::move(state), hihat::init_parameters));
  return controller.release();
}

Steinberg::FUnknown* createCbController(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  cb::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IEditController> controller;

  controller.reset(new DrumController("cb", "drums.uidesc", std::move(state), cb::init_parameters));
  return controller.release();
}

}  // namespace drumelidrum
