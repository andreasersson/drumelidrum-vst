/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_DRUM_CONTROLLER_H
#define DRUMELIDRUM_DRUM_CONTROLLER_H

#include <filurvst/common/parameter_state.h>
#include <filurvst/gui/aboutbox_controller.h>

#include <public.sdk/source/vst/vsteditcontroller.h>
#include <pluginterfaces/base/smartpointer.h>
#include <vstgui/plugin-bindings/vst3editor.h>

#include <functional>
#include <memory>
#include <string>

namespace drumelidrum {

class DrumController : public Steinberg::Vst::EditControllerEx1, public  VSTGUI::VST3EditorDelegate {
 public:
  DrumController(const std::string& template_name,
                 const std::string& uidesc,
                 std::unique_ptr<filurvst::State> state,
                 std::function<void(Steinberg::Vst::ParameterContainer&)> initialize)
 : EditControllerEx1(),
   m_template_name(template_name),
   m_uidesc(uidesc),
   m_state(std::move(state)),
   m_initialize(initialize),
   m_aboutbox(new filurvst::gui::AboutBoxController, false) {}

  Steinberg::tresult initialize(FUnknown* context) SMTG_OVERRIDE;
  Steinberg::tresult setComponentState(Steinberg::IBStream* state) SMTG_OVERRIDE;
  Steinberg::IPlugView* createView(Steinberg::FIDString name) SMTG_OVERRIDE;

  virtual Steinberg::tresult PLUGIN_API openAboutBox(Steinberg::TBool onlyCheck) SMTG_OVERRIDE;

  virtual VSTGUI::CView* createCustomView(VSTGUI::UTF8StringPtr name,
                                          const VSTGUI::UIAttributes& attributes,
                                          const VSTGUI::IUIDescription* description,
                                          VSTGUI::VST3Editor* editor) override;
  virtual VSTGUI::COptionMenu* createContextMenu(const VSTGUI::CPoint& pos,
                                                 VSTGUI::VST3Editor* editor) override;

  OBJ_METHODS (DrumController, EditControllerEx1)DEFINE_INTERFACES
  DEF_INTERFACE (IUnitInfo)
  END_DEFINE_INTERFACES (EditControllerEx1)
  REFCOUNT_METHODS(EditControllerEx1)

private:
  std::string m_template_name;
  std::string m_uidesc;
  std::unique_ptr<filurvst::State> m_state;
  std::function<void(Steinberg::Vst::ParameterContainer&)> m_initialize;

 Steinberg::IPtr<filurvst::gui::AboutBoxController> m_aboutbox;
};

Steinberg::FUnknown* createBdController(void*);
Steinberg::FUnknown* createTomController(void*);
Steinberg::FUnknown* createKickController(void*);
Steinberg::FUnknown* createSdController(void*);
Steinberg::FUnknown* createClapController(void*);
Steinberg::FUnknown* createCymbalController(void*);
Steinberg::FUnknown* createHihatController(void*);
Steinberg::FUnknown* createCbController(void*);

}  // namespace drumelidrum

#endif // DRUMELIDRUM_DRUM_CONTROLLER_H
