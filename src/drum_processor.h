/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_DRUM_PROCESSOR_H
#define DRUMELIDRUM_DRUM_PROCESSOR_H

#include "module.h"
#include "modules/pitch_to_frequency.h"

#include <filurvst/common/parameter_state.h>

#include <filur/drums/bd.h>
#include <filur/drums/sd.h>
#include <filur/drums/clap.h>
#include <filur/drums/cymbal.h>
#include <filur/drums/hihat.h>
#include <filur/drums/cb.h>

#include <public.sdk/source/vst/vstaudioeffect.h>
#include <pluginterfaces/base/funknown.h>
#include <pluginterfaces/vst/vsttypes.h>

#include <vector>
#include <memory>

namespace drumelidrum {

class DrumProcessor : public Steinberg::Vst::AudioEffect {
 public:
  DrumProcessor(const Steinberg::FUID& controller_uid,
                std::unique_ptr<filurvst::State> state,
                std::unique_ptr<module::ModuleIf> module);

  Steinberg::tresult initialize(Steinberg::FUnknown* context) override;
  Steinberg::tresult setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                        Steinberg::int32 numIns,
                                        Steinberg::Vst::SpeakerArrangement* outputs,
                                        Steinberg::int32 numOuts) override;

  Steinberg::tresult setState(Steinberg::IBStream* state) override;
  Steinberg::tresult getState(Steinberg::IBStream* state) override;

  Steinberg::tresult canProcessSampleSize(Steinberg::int32 symbolicSampleSize) override;
  Steinberg::tresult setActive(Steinberg::TBool state) override;
  Steinberg::tresult process(Steinberg::Vst::ProcessData& data) override;

 private:
  std::vector<double> m_left_buffer;
  std::vector<double> m_right_buffer;

  std::unique_ptr<filurvst::State> m_state;
  std::unique_ptr<module::ModuleIf> m_module;

  Steinberg::TBool m_is_active = {0};
};

namespace bd {
static const Steinberg::FUID puid(0x98E10040, 0x1C6C4522, 0x84FEF3C5, 0xF86C83BB);
static const Steinberg::FUID cuid(0xBE6DB38D, 0xAB1E407B, 0x994432BC, 0x43CE6648);

class Bd : public module::ModuleIf {
 public:
  Bd()
 : m_low_note(0),
   m_high_note(127),
   m_pitch(0) {}

  virtual ~Bd() {
  }

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int /*note_nr*/) override {}
  virtual void choke() override {}
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override { return (BD_SILENT_TRUE == m_bd.silent); }
  virtual double level() const override { return 0; }

  virtual void setParameter(int32_t parameter_id, double value) override;
  virtual module::ParameterIf* getParameters() override { return nullptr; }

 protected:
  int m_low_note;
  int m_high_note;
  int m_pitch;
  bd_parameters_t m_bd_parameters = {};
  bd_t m_bd  = {};
  module::PitchToFrequency m_pitch_to_frequency;
};

}  // namespace bd

namespace tom {
static const Steinberg::FUID puid(0xD2600A62, 0xEDE840C2, 0x9728CE3C, 0xB14342A5);
static const Steinberg::FUID cuid(0x468C3A8F, 0x497C4E35, 0xB14E913A, 0x60866D74);

class Tom : public bd::Bd {
 public:
  Tom() : Bd() {}
  virtual ~Tom() {}

  virtual void noteOn(int note_nr, double velocity) override;
};

}  // namespace tom

namespace kick {
static const Steinberg::FUID puid(0x3750113A, 0x45F44C5D, 0x905916DC, 0x1595A656);
static const Steinberg::FUID cuid(0x6766AA1A, 0xDD904406, 0x9DF01626, 0x34845DEB);

class Kick : public bd::Bd {
 public:
  Kick() : Bd() {}
  virtual ~Kick() {}

  virtual void noteOn(int note_nr, double velocity) override;
};

}  // namespace kick

namespace sd {
static const Steinberg::FUID puid(0xC6038DA9, 0x620748D5, 0x8413FE12, 0x6D68DE01);
static const Steinberg::FUID cuid(0x898C39EF, 0x1F664833, 0x9D2D2C99, 0xA800BE19);

class Sd : public module::ModuleIf {
 public:
  Sd()
 : m_low_note(0),
   m_high_note(127),
   m_pitch(0) {}
  virtual ~Sd() {}

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int /*note_nr*/) override {}
  virtual void choke() override {}
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override { return (SD_SILENT_TRUE == m_sd.silent); }
  virtual double level() const override { return 0; }

  virtual void setParameter(int32_t parameter_id, double value) override;
  virtual module::ParameterIf* getParameters() override { return nullptr; }

 protected:
  int m_low_note;
  int m_high_note;
  int m_pitch;
  sd_t m_sd = {};
  module::PitchToFrequency m_pitch_to_frequency;
};

}  // namespace sd

namespace clap {
static const Steinberg::FUID puid(0x1E55473C, 0x3208413D, 0x9F259880, 0xA00CE764);
static const Steinberg::FUID cuid(0x880D9749, 0x91B44727, 0x96180B4F, 0x37BFF9B1);

class Clap : public module::ModuleIf {
 public:
  Clap()
 : m_low_note(0),
   m_high_note(127) {}
  virtual ~Clap() {}

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int /*note_nr*/) override {}
  virtual void choke() override {}
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override { return (CLAP_SILENT_TRUE == m_clap.silent); }
  virtual double level() const override { return 0; }

  virtual void setParameter(int32_t parameter_id, double value) override;
  virtual module::ParameterIf* getParameters() override { return nullptr; }

 protected:
  int m_low_note;
  int m_high_note;
  clap_parameters_t m_clap_parameters = {};
  clap_t m_clap = {};
};

}  // namespace clap

namespace cymbal {
static const Steinberg::FUID puid(0xEEC485B1, 0x6E8F49CD, 0xA2021D43, 0xDEDFFE25);
static const Steinberg::FUID cuid(0xC04B9E44, 0xE8AD4682, 0x85CC0ED3, 0x81D47D3A);

class Cymbal : public module::ModuleIf {
 public:
  Cymbal()
 : m_low_note(0),
   m_high_note(127),
   m_pitch(0) {}
  virtual ~Cymbal() {}

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int /*note_nr*/) override {}
  virtual void choke() override {}
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override { return (CYMBAL_SILENT_TRUE == m_cymbal.silent); }
  virtual double level() const override { return 0; }

  virtual void setParameter(int32_t parameter_id, double value) override;
  virtual module::ParameterIf* getParameters() override { return nullptr; }

 protected:
  int m_low_note;
  int m_high_note;
  int m_pitch;
  cymbal_parameters_t m_cymbal_parameters = {};
  cymbal_t m_cymbal = {};
  module::PitchToFrequency m_pitch_to_frequency;
};

}  // namespace cymbal

namespace hihat {
static const Steinberg::FUID puid(0x93B83BB9, 0xDD1F4A10, 0xB81A6E43, 0xB5DEFE75);
static const Steinberg::FUID cuid(0x3C052D21, 0x96954C97, 0xA2882166, 0xAECE3127);

class Hihat : public module::ModuleIf {
 public:
  Hihat()
 : m_closed_note(42),
   m_open_note(46),
   m_pedal_note(44),
   m_pitch(36) {}
  virtual ~Hihat() {}

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int /*note_nr*/) override {}
  virtual void choke() override {}
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override { return (HIHAT_SILENT_TRUE == m_hihat.silent); }
  virtual double level() const override { return 0; }

  virtual void setParameter(int32_t parameter_id, double value) override;
  virtual module::ParameterIf* getParameters() override { return nullptr; }

 public:
  int m_closed_note;
  int m_open_note;
  int m_pedal_note;
  int m_pitch;
  hihat_parameters_t m_hihat_parameters = {};
  hihat_t m_hihat = {};
  module::PitchToFrequency m_pitch_to_frequency;
};

}  // namespace hihat

namespace cb {
static const Steinberg::FUID puid(0x222D98D, 0x2A0341DE, 0xB21D9727, 0xA421A113);
static const Steinberg::FUID cuid(0x4F0DD5A8, 0xFCB4E47, 0xB3C23DAA, 0x1E1C11A0);

class Cb : public module::ModuleIf {
 public:
  Cb()
 : m_low_note(0),
   m_high_note(127),
   m_pitch(0) {}
  virtual ~Cb() {}

  virtual void initialize(double sample_rate, size_t max_block_size) override;
  virtual void noteOn(int note_nr, double velocity) override;
  virtual void noteOff(int /*note_nr*/) override {}
  virtual void choke() override {}
  virtual void process(double *left, double *right, int number_of_frames) override;
  virtual bool silent() override { return (CB_SILENT_TRUE == m_cb.silent); }
  virtual double level() const override { return 0; }

  virtual void setParameter(int32_t parameter_id, double value) override;
  virtual module::ParameterIf* getParameters() override { return nullptr; }

 protected:
  int m_low_note;
  int m_high_note;
  int m_pitch;
  cb_t m_cb = {};
  module::PitchToFrequency m_pitch_to_frequency;
};

}  // namespace cb

Steinberg::FUnknown* createBdProcessor(void*);
Steinberg::FUnknown* createTomProcessor(void*);
Steinberg::FUnknown* createKickProcessor(void*);
Steinberg::FUnknown* createSdProcessor(void*);
Steinberg::FUnknown* createClapProcessor(void*);
Steinberg::FUnknown* createCymbalProcessor(void*);
Steinberg::FUnknown* createHihatProcessor(void*);
Steinberg::FUnknown* createCbProcessor(void*);

}  // namespace drumelidrum

#endif // DRUMELIDRUM_DRUM_PROCESSOR_H
