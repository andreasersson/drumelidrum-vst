/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_CONTROLLER_H
#define DRUMELIDRUM_CONTROLLER_H

#include <filurvst/gui/aboutbox_controller.h>

#include <public.sdk/source/vst/vsteditcontroller.h>
#include <pluginterfaces/base/smartpointer.h>
#include <vstgui/plugin-bindings/vst3editor.h>

namespace drumelidrum {

class Controller : public Steinberg::Vst::EditControllerEx1, public  VSTGUI::VST3EditorDelegate {
 public:
  Controller();

  Steinberg::tresult initialize(FUnknown* context) SMTG_OVERRIDE;
  Steinberg::tresult terminate() override;
  Steinberg::tresult setComponentState(Steinberg::IBStream* state) SMTG_OVERRIDE;

  Steinberg::tresult setParamNormalized(Steinberg::Vst::ParamID tag,
                                        Steinberg::Vst::ParamValue value) SMTG_OVERRIDE;
  Steinberg::IPlugView* createView(Steinberg::FIDString name) SMTG_OVERRIDE;

  virtual Steinberg::tresult PLUGIN_API openAboutBox(Steinberg::TBool onlyCheck) SMTG_OVERRIDE;

  virtual VSTGUI::CView* createCustomView(VSTGUI::UTF8StringPtr name,
                                          const VSTGUI::UIAttributes& attributes,
                                          const VSTGUI::IUIDescription* description,
                                          VSTGUI::VST3Editor* editor) override;
  virtual VSTGUI::COptionMenu* createContextMenu(const VSTGUI::CPoint& pos,
                                                 VSTGUI::VST3Editor* editor) override;

  static Steinberg::FUID cid;

  OBJ_METHODS (Controller, EditControllerEx1)DEFINE_INTERFACES
  DEF_INTERFACE (IUnitInfo)
  END_DEFINE_INTERFACES (EditControllerEx1)
  REFCOUNT_METHODS(EditControllerEx1)

 private:
  Steinberg::IPtr<filurvst::gui::AboutBoxController> m_aboutbox;
};

} // namespace drumelidrum

#endif // DRUMELIDRUM_CONTROLLER_H
