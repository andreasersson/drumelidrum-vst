/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_MODULE_FACTORY_H
#define DRUMELIDRUM_MODULE_FACTORY_H

#include "module_types.h"
#include "parameter_state.h"
#include "modules/modules.h"

#include <memory>

namespace drumelidrum {

std::unique_ptr<module::DrumPad> drumpad_module(filurvst::State &parameter_state,
                                                int id,
                                                module::ChokeIf* choke = nullptr);

std::unique_ptr<module::ModuleIf> bd_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> kick_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> tom_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> sd_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> rs_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> claves_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> clap_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> ch_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> ph_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> oh_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> cb_module(filurvst::State &parameter_state, int pad_id);
std::unique_ptr<module::ModuleIf> cymbal_module(filurvst::State &parameter_state, int pad_id);

std::unique_ptr<module::ModuleIf> module_factory(filurvst::State &parameter_state,
                                                 int pad_id,
                                                 module_type_t type);

}  // namespace drumelidrum

#endif // DRUMELIDRUM_MODULE_FACTORY_H
