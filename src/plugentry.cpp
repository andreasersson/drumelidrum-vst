/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "controller.h"
#include "processor.h"
#include "version.h"

#include <public.sdk/source/main/pluginfactory.h>

#include "drum_controller.h"
#include "drum_processor.h"

static Steinberg::FUnknown* createProcessorInstance(void*) {
  return static_cast<Steinberg::Vst::IAudioProcessor*>(new drumelidrum::Processor(drumelidrum::Controller::cid));
}

static Steinberg::FUnknown* createControllerInstance(void*) {
  return static_cast<Steinberg::Vst::IEditController*>(new drumelidrum::Controller());
}

BEGIN_FACTORY_DEF (stringCompanyName, stringCompanyWeb,  stringCompanyEmail)
    DEF_CLASS2(INLINE_UID_FROM_FUID(drumelidrum::Processor::cid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               stringPluginName,
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               createProcessorInstance)
    DEF_CLASS2(INLINE_UID_FROM_FUID(drumelidrum::Controller::cid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               stringPluginName,
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               createControllerInstance)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::bd::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum bd",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createBdProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::bd::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum bd",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createBdController)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::tom::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum tom",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createTomProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::tom::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum tom",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createTomController)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::kick::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum kick",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createKickProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::kick::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum kick",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createKickController)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::sd::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum sd",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createSdProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::sd::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum sd",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createSdController)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::clap::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum clap",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createClapProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::clap::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum clap",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createClapController)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::cymbal::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum cymbal",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createCymbalProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::cymbal::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum cymbal",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createCymbalController)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::hihat::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum hihat",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createHihatProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::hihat::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum hihat",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createHihatController)

    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::cb::puid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               "drumelidrum cb",
               Vst::kDistributable,
               Vst::PlugType::kInstrumentDrum,
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createCbProcessor)
    DEF_CLASS2(INLINE_UID_FROM_FUID (drumelidrum::cb::cuid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               "drumelidrum cb",
               0,
               "",
               FULL_VERSION_STR,
               kVstVersionString,
               drumelidrum::createCbController)
END_FACTORY

bool InitModule() {
  return true;
}

bool DeinitModule() {
  return true;
}
