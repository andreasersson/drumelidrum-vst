/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "parameter_state.h"

#include "drumpad_ids.h"
#include "midi_drum_map.h"
#include "module_parameters.h"
#include "module_types.h"
#include "parameter_ids.h"
#include "modules/modules.h"

#include <cassert>

namespace drumelidrum {

static void update_module_parameters(ParameterState& state);

ParameterState::ParameterState() {
  const static std::map<int, int> default_pad_note_map = {
      { kAcousticBassDrumPad, kAcousticBassDrum },
      { kBassDrum1Pad, kBassDrum1 },
      { kAcousticSnarePad, kAcousticSnare },
      { kElectricSnarePad, kElectricSnare },
      { kHandClapPad, kHandClap },
      { kClosedHihatPad, kClosedHihat },
      { kPedalHihatPad, kPedalHihat },
      { kOpenHihatPad, kOpenHihat },
      { kSideStickPad, kSideStick },
      { kLowFloorTomPad, kLowFloorTom },
      { kHighFloorTomPad, kHighFloorTom },
      { kLowTomPad, kLowTom },
      { kLowMidTomPad, kLowMidTom },
      { kHiMidTomPad, kHiMidTom },
      { kClavesPad, kClaves },
      { kCbPad, kCowbell },
      { kHighTomPad, kHighTom },
      { kCrashPad, kCrashCymbal1 },
      { kRidePad, kRideCymbal1 },
      { kHiBongoPad, kHiBongo },
      { kLowBongoPad, kLowBongo },
      { kRideBellPad, kRideBell },
      { kPrc3Pad, kLowConga },
      { kClap2Pad, kCabasa } };

  const static std::map<int, module_type_t> default_module_type_map = {
      { kAcousticBassDrumPad, kModuleBd },
      { kBassDrum1Pad, kModuleBd },
      { kAcousticSnarePad, kModuleSd },
      { kElectricSnarePad, kModuleSd },
      { kHandClapPad, kModuleClap },
      { kClosedHihatPad, kModuleClosedHihat },
      { kPedalHihatPad, kModulePedalHihat },
      { kOpenHihatPad, kModuleOpenHihat },
      { kSideStickPad, kModuleRs },
      { kLowFloorTomPad, kModuleKick },
      { kHighFloorTomPad, kModuleKick },
      { kLowTomPad, kModuleTom },
      { kLowMidTomPad, kModuleTom },
      { kHiMidTomPad, kModuleTom },
      { kClavesPad, kModuleClaves },
      { kCbPad, kModuleCowbell },
      { kHighTomPad, kModuleTom },
      { kCrashPad, kModuleCymbal },
      { kRidePad, kModuleCymbal },
      { kHiBongoPad, kModuleSd },
      { kLowBongoPad, kModuleSd },
      { kRideBellPad, kModuleCymbal },
      { kPrc3Pad, kModuleSd },
      { kClap2Pad, kModuleClap } };

  const double default_channel = 0;
  const double default_volume = 0.85;
  const double default_pan = 0.5;

  // Add drum pad parameters
  for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
    int pad_base = pad_id * kPadOffset;
    addUInt(pad_base + kPadModule, default_module_type_map.at(pad_id), 0, kModuleNumTypes - 1);
    addUInt(pad_base + kPadChannel, default_channel, 0, kNumMaxchannels);
    add(pad_base + kPadVolume, default_volume);
    add(pad_base + kPadPan, default_pan);
    addUInt(pad_base + kPadNote, default_pad_note_map.at(pad_id), 0, kPadMaxNote);
    addUInt(pad_base + kPadNoteEnd, default_pad_note_map.at(pad_id), 0, kPadMaxNote);
    addUInt(pad_base + kPadChoke, 0, 0, 0xffffffff);

    for (int parameter = 0; parameter < 8; ++parameter) {
      add(pad_base + kMaxNumPadParameters + parameter, 0.0);
    }
  }

  update_module_parameters(*this);
}

Steinberg::tresult ParameterState::setState(Steinberg::IBStream* stream) {
  Steinberg::tresult ret_val = filurvst::State::setState(stream);

  if (Steinberg::kResultTrue == ret_val) {
    update_module_parameters(*this);
  }

  return ret_val;
}

static void update_module_parameters(ParameterState& state) {
  for (int pad_id = 0; pad_id < kNumPads; ++pad_id) {
    int pad_parameter_base = pad_id * kPadOffset;
    int module_type = static_cast<int>(state.get(pad_parameter_base));

    switch (module_type) {
      case kModuleBd:
        module::BdParameters::updateParameters(state, bd_parameter_map(pad_id));
        break;

      case kModuleSd:
        module::SdParameters::updateParameters(state, sd_parameter_map(pad_id));
        break;

      case kModuleRs:
        module::RsParameters::updateParameters(state, sd_parameter_map(pad_id));
        break;

      case kModuleClosedHihat:
        module::HihatParameters::updateParameters(state, closed_hihat_parameter_map(pad_id));
        break;

      case kModulePedalHihat:
        module::HihatParameters::updateParameters(state, pedal_hihat_parameter_map(pad_id));
        break;

      case kModuleOpenHihat:
        module::HihatParameters::updateParameters(state, open_hihat_parameter_map(pad_id));
        break;

      case kModuleClap:
        module::ClapParameters::updateParameters(state, clap_parameter_map(pad_id));
        break;

      case kModuleCowbell:
        module::CbParameters::updateParameters(state, cb_parameter_map(pad_id));
        break;

      case kModuleClaves:
        module::ClavesParameters::updateParameters(state, sd_parameter_map(pad_id));
        break;

      case kModuleKick:
        module::TomParameters::updateParameters(state, tom_parameter_map(pad_id));
        break;

      case kModuleTom:
        module::TomParameters::updateParameters(state, tom_parameter_map(pad_id));
        break;

      case kModuleCymbal:
        module::CymbalParameters::updateParameters(state, cymbal_parameter_map(pad_id));
        break;

      default:
        assert(false);
        break;
    }
  }
}

void default_parameters(ParameterState& state) {
  enum {
    // Bd1
    kBd1Pitch = kAcousticBassDrumPad * kPadOffset + kMaxNumPadParameters,
    kBd1Tone,
    kBd1Decay,
    kBd1SweepDecay,
    kBd1Sweep,
    kBd1SweepThreshold,

    // Bd2
    kBd2Pitch = kBassDrum1Pad * kPadOffset + kMaxNumPadParameters,
    kBd2Tone,
    kBd2Decay,
    kBd2SweepDecay,
    kBd2Sweep,
    kBd2SweepThreshold,

    // Rs
    kRsPitch = kSideStickPad * kPadOffset + kMaxNumPadParameters,
    kRsTone,
    kRsSnappy,
    kRsDecay,

    // Sd1
    kSd1Pitch = kAcousticSnarePad * kPadOffset + kMaxNumPadParameters,
    kSd1Tone,
    kSd1Snappy,
    kSd1Decay,

    // Clap
    kClapTone = kHandClapPad * kPadOffset + kMaxNumPadParameters,
    kClapReso,
    kClapDecay,
    kClapEarlyDecay,
    kClapEarlyDelay,

    // Sd2
    kSd2Pitch = kElectricSnarePad * kPadOffset + kMaxNumPadParameters,
    kSd2Tone,
    kSd2Snappy,
    kSd2Decay,

    // LowFloorTom
    kLowFloorTomPitch = kLowFloorTomPad * kPadOffset + kMaxNumPadParameters,
    kLowFloorTomTone,
    kLowFloorTomDecay,
    kLowFloorTomSweepDecay,
    kLowFloorTomSweep,
    kLowFloorTomSweepThreshold,

    // Closed Hihat
    kClosedHihatPitch = kClosedHihatPad * kPadOffset + kMaxNumPadParameters,
    kClosedHihatNoise,
    kClosedHihatTone,
    kClosedHihatReso,
    kClosedHihatDecay,

    // Pedal Hihat
    kPedalHihatPitch = kPedalHihatPad * kPadOffset + kMaxNumPadParameters,
    kPedalHihatNoise,
    kPedalHihatTone,
    kPedalHihatReso,
    kPedalHihatDecay,

    // Open Hihat
    kOpenHihatPitch = kOpenHihatPad * kPadOffset + kMaxNumPadParameters,
    kOpenHihatNoise,
    kOpenHihatTone,
    kOpenHihatReso,
    kOpenHihatDecay,

    // tom
    kHighFloorTomPitch = kHighFloorTomPad * kPadOffset + kMaxNumPadParameters,
    kHighFloorTomTone,
    kHighFloorTomDecay,
    kHighFloorTomSweepDecay,
    kHighFloorTomSweep,
    kHighFloorTomSweepThreshold,

    // tom
    kLowTomPitch = kLowTomPad * kPadOffset + kMaxNumPadParameters,
    kLowTomTone,
    kLowTomDecay,
    kLowTomSweepDecay,
    kLowTomSweep,
    kLowTomSweepThreshold,

    // tom
    kLowMidTomPitch = kLowMidTomPad * kPadOffset + kMaxNumPadParameters,
    kLowMidTomTone,
    kLowMidTomDecay,
    kLowMidTomSweepDecay,
    kLowMidTomSweep,
    kLowMidTomSweepThreshold,

    // tom
    kHiMidTomPitch = kHiMidTomPad * kPadOffset + kMaxNumPadParameters,
    kHiMidTomTone,
    kHiMidTomDecay,
    kHiMidTomSweepDecay,
    kHiMidTomSweep,
    kHiMidTomSweepThreshold,

    // tom
    kHiTomPitch = kHighTomPad * kPadOffset + kMaxNumPadParameters,
    kHiTomTone,
    kHiTomDecay,
    kHiTomSweepDecay,
    kHiTomSweep,
    kHiTomSweepThreshold,

    // claves
    kClavesPitch = kClavesPad * kPadOffset + kMaxNumPadParameters,
    kClavesTone,
    kClavesSnappy,
    kClavesDecay,

    // Cb
    kCbPitch = kCbPad * kPadOffset + kMaxNumPadParameters,
    kCbDecay,

    // Crash
    kCrashPitch = kCrashPad * kPadOffset + kMaxNumPadParameters,
    kCrashTone,
    kCrashReso,
    kCrashEarlyDecay,
    kCrashLateDecay,
    kCrashDecayThreshold,
    kCrashNoise,

    // Ride
    kRidePitch = kRidePad * kPadOffset + kMaxNumPadParameters,
    kRideTone,
    kRideReso,
    kRideEarlyDecay,
    kRideLateDecay,
    kRideDecayThreshold,
    kRideNoise,

    // RideBell
    kRideBellPitch = kRideBellPad * kPadOffset + kMaxNumPadParameters,
    kRideBellTone,
    kRideBellReso,
    kRideBellEarlyDecay,
    kRideBellLateDecay,
    kRideBellDecayThreshold,
    kRideBellNoise,

    kLowBongoPitch = kLowBongoPad * kPadOffset + kMaxNumPadParameters,
    kLowBongoTone,
    kLowBongoSnappy,
    kLowBongoDecay,

    kHiBongoPitch = kHiBongoPad * kPadOffset + kMaxNumPadParameters,
    kHiBongoTone,
    kHiBongoSnappy,
    kHiBongoDecay,

    kDrum23Pitch = kPrc3Pad * kPadOffset + kMaxNumPadParameters,

    // Clap 2
    kClap2Tone = kClap2Pad * kPadOffset + kMaxNumPadParameters,
    kClap2Reso,
    kClap2Decay,
    kClap2EarlyDecay,
    kClap2EarlyDelay
  };

  // Set default values
  // Bd1
  state.set(kBd1Pitch, 29);
  state.set(kBd1Tone, 0.75);
  state.set(kBd1Decay, 1.5);
  state.set(kBd1SweepDecay, 500e-3);
  state.set(kBd1Sweep, 1.0);
  state.set(kBd1SweepThreshold, 0.055);

  // Bd2
  state.set(kBd2Pitch, 29);
  state.set(kBd2Tone, 0.25);
  state.set(kBd2Decay, 400e-3);
  state.set(kBd2SweepDecay, 500e-3);
  state.set(kBd2Sweep, 1.0);
  state.set(kBd2SweepThreshold, 0.055);

  // Sd1
  state.set(kSd1Pitch, 48);
  state.set(kSd1Tone, 0.75);
  state.set(kSd1Snappy, 0.55);
  state.set(kSd1Decay, 200e-3);

  // Sd2
  state.set(kSd2Pitch, 53);
  state.set(kSd2Tone, 0.75);
  state.set(kSd2Snappy, 0.50);
  state.set(kSd2Decay, 300e-3);

  // Rs
  state.set(kRsPitch, 72);
  state.set(kRsTone, 0.5);
  state.set(kRsSnappy, 0.2);
  state.set(kRsDecay, 50e-3);

  // Hihat
  state.set(kClosedHihatPitch, 36);
  state.set(kClosedHihatNoise, METAL_NOISE_TYPE_1);
  state.set(kClosedHihatTone, 0.4);
  state.set(kClosedHihatReso, 0.5);
  state.set(kClosedHihatDecay, 0.09);
  state.set(kOpenHihatDecay, 2.0);
  state.set(kPedalHihatDecay, 0.40);

  // Clap
  state.set(kClapTone, 0.4);
  state.set(kClapReso, 0.6);
  state.set(kClapDecay, 240e-3);
  state.set(kClapEarlyDecay, 50e-3);
  state.set(kClapEarlyDelay, 15e-3);

  // Clap 2
  state.set(kClap2Tone, 0.62);
  state.set(kClap2Reso, 0.6);
  state.set(kClap2Decay, 240e-3);
  state.set(kClap2EarlyDecay, 50e-3);
  state.set(kClap2EarlyDelay, 15e-3);

  // Crash
  state.set(kCrashPitch, 36);
  state.set(kCrashTone, 0.21);
  state.set(kCrashReso, 1.0);
  state.set(kCrashEarlyDecay, 0.05);
  state.set(kCrashLateDecay, 4.000);
  state.set(kCrashDecayThreshold, 0.55);
  state.set(kCrashNoise, METAL_NOISE_TYPE_4);

  // Ride
  state.set(kRidePitch, 48);
  state.set(kRideTone, 0.5);
  state.set(kRideReso, 0.33);
  state.set(kRideEarlyDecay, 0.07);
  state.set(kRideLateDecay, 3.000);
  state.set(kRideDecayThreshold, 0.30);
  state.set(kRideNoise, METAL_NOISE_TYPE_4);

  // RideBell
  state.set(kRideBellPitch, 60);
  state.set(kRideBellTone, 0.45);
  state.set(kRideBellReso, 0.5);
  state.set(kRideBellEarlyDecay, 0.07);
  state.set(kRideBellLateDecay, 2.000);
  state.set(kRideBellDecayThreshold, 0.5);
  state.set(kRideBellNoise, METAL_NOISE_TYPE_4);

  // LowFloorTom
  state.set(kLowFloorTomPitch, 29);
  state.set(kLowFloorTomTone, 0.95);
  state.set(kLowFloorTomDecay, 1.0);
  state.set(kLowFloorTomSweepDecay, 500e-3);
  state.set(kLowFloorTomSweep, 1.0);
  state.set(kLowFloorTomSweepThreshold, 0.055);

  // LowFloorTom
  state.set(kHighFloorTomPitch, 31);
  state.set(kHighFloorTomTone, 0.95);
  state.set(kHighFloorTomDecay, 1.0);
  state.set(kHighFloorTomSweepDecay, 500e-3);
  state.set(kHighFloorTomSweep, 1.0);
  state.set(kHighFloorTomSweepThreshold, 0.055);

  // LowTom
  state.set(kLowTomPitch, 45);
  state.set(kLowTomTone, 0.25);
  state.set(kLowTomDecay, 0.75);
  state.set(kLowTomSweepDecay, 500e-3);
  state.set(kLowTomSweep, 1.0);
  state.set(kLowTomSweepThreshold, 0.055);

  // LowMidTom
  state.set(kLowMidTomPitch, 47);
  state.set(kLowMidTomTone, 0.25);
  state.set(kLowMidTomDecay, 0.75);
  state.set(kLowMidTomSweepDecay, 500e-3);
  state.set(kLowMidTomSweep, 1.0);
  state.set(kLowMidTomSweepThreshold, 0.055);

  // HiMidTom
  state.set(kHiMidTomPitch, 49);
  state.set(kHiMidTomTone, 0.25);
  state.set(kHiMidTomDecay, 0.75);
  state.set(kHiMidTomSweepDecay, 500e-3);
  state.set(kHiMidTomSweep, 1.0);
  state.set(kHiMidTomSweepThreshold, 0.055);

  // HiTomPitch
  state.set(kHiTomPitch, 55);
  state.set(kHiTomTone, 0.25);
  state.set(kHiTomDecay, 0.75);
  state.set(kHiTomSweepDecay, 500e-3);
  state.set(kHiTomSweep, 1.0);
  state.set(kHiTomSweepThreshold, 0.055);

  // Claves
  state.set(kClavesPitch, 84);
  state.set(kClavesTone, 0.35);
  state.set(kClavesSnappy, 0.0);
  state.set(kClavesDecay, 20e-3);

  // Cb
  state.set(kCbPitch, 60);
  state.set(kCbDecay, 200e-3);

  //
  state.set(kLowBongoPitch, 66);
  state.set(kLowBongoTone, 0.75);
  state.set(kLowBongoSnappy, 0.50);
  state.set(kLowBongoDecay, 300e-3);

  state.set(kHiBongoPitch, 69);
  state.set(kHiBongoTone, 0.75);
  state.set(kHiBongoSnappy, 0.50);
  state.set(kHiBongoDecay, 300e-3);
}

}  // namespace drumelidrum

