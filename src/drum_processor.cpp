/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "drum_processor.h"

#include "drum_parameters.h"

#include <pluginterfaces/base/ustring.h>
#include <pluginterfaces/vst/ivstevents.h>
#include <pluginterfaces/vst/ivstparameterchanges.h>

#include <algorithm>
#include <cassert>
#include <cmath>

namespace drumelidrum {

static bool in_range(int note_nr, int low, int high) {
  return ((note_nr >= low) && (note_nr <= high));
}

static bool in_range(int note_nr, int low, int high, int base_pitch, int& pitch) {
  bool note_in_range =  in_range(note_nr, low, high);
  if (note_in_range) {
    pitch = (note_nr - low) + base_pitch;
  }

  return note_in_range;
}

static int pitch_value(double value) {
  return std::min(127, std::max(0, static_cast<int>(value)));
}

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                                     filurvst::State* state,
                                     module::ModuleIf* module);
static void set_parameter(Steinberg::Vst::ParamID parameter_id, double value, module::ModuleIf* module);
static void set_state_parameters(filurvst::State* state, module::ModuleIf* module);
static void to_sample32(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data);
static void to_sample64(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data);

namespace bd {

void Bd::initialize(double sample_rate, size_t /*max_block_size*/) {
  bd_init_parameters(&m_bd_parameters);
  bd_init(sample_rate, &m_bd);
}

void Bd::noteOn(int note_nr, double velocity) {
  int pitch = 0;
  if (in_range(note_nr, m_low_note, m_high_note, m_pitch, pitch)) {
    bd_set_gain(velocity, &m_bd_parameters);
    bd_set_frequency(m_pitch_to_frequency.frequency(pitch), &m_bd_parameters);
    bd_note_on_classic(&m_bd_parameters, &m_bd);
  }
}

void Bd::process(double *left, double *right, int number_of_frames) {
  bd_process(left, number_of_frames, &m_bd_parameters, &m_bd);
  memcpy(right, left, number_of_frames * sizeof(double));
}

void Bd::setParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kLowNote:
      m_low_note = pitch_value(value);
      break;

    case kHighNote:
      m_high_note = pitch_value(value);
      break;

    case kPitch:
      m_pitch = pitch_value(value);
      break;

    case kTone:
      bd_set_tone(value, &m_bd_parameters);
      break;

    case kDecay:
      bd_set_decay(value, &m_bd_parameters);
      break;

    case kSweepDecay:
      bd_set_sweep_decay(value, &m_bd_parameters);
      break;

    case kSweep:
      bd_set_sweep(value, &m_bd_parameters);
      break;

    case kSweepThreshold:
      bd_set_sweep_threshold(value, &m_bd_parameters);
      break;

    default:
      assert(false);
      break;
  };
}

}  // namespace bd

namespace tom {

void Tom::noteOn(int note_nr, double velocity) {
  int pitch = 0;
  if (in_range(note_nr, m_low_note, m_high_note, m_pitch, pitch)) {
    bd_set_gain(velocity, &m_bd_parameters);
    bd_set_frequency(m_pitch_to_frequency.frequency(pitch), &m_bd_parameters);
    bd_note_on_tom(&m_bd_parameters, &m_bd);
  }
}

}  // namespace tom

namespace kick {

void Kick::noteOn(int note_nr, double velocity) {
  int pitch = 0;
  if (in_range(note_nr, m_low_note, m_high_note, m_pitch, pitch)) {
    bd_set_gain(velocity, &m_bd_parameters);
    bd_set_frequency(m_pitch_to_frequency.frequency(pitch), &m_bd_parameters);
    bd_note_on_kick(&m_bd_parameters, &m_bd);
  }
}

}  // namespace kick

namespace sd {

void Sd::initialize(double sample_rate, size_t /*max_block_size*/) {
  sd_init(sample_rate, &m_sd);
}

void Sd::noteOn(int note_nr, double velocity) {
  int pitch = 0;
  if (in_range(note_nr, m_low_note, m_high_note, m_pitch, pitch)) {
    sd_set_gain(velocity, &m_sd);
    sd_set_frequency(m_pitch_to_frequency.frequency(pitch), &m_sd);
    sd_note_on(&m_sd);
  }
}

void Sd::process(double *left, double *right, int number_of_frames) {
  sd_process(left, number_of_frames, &m_sd);
  memcpy(right, left, number_of_frames * sizeof(double));
}

void Sd::setParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kLowNote:
      m_low_note = pitch_value(value);
      break;

    case kHighNote:
      m_high_note = pitch_value(value);
      break;

    case kPitch:
      m_pitch = pitch_value(value);
      break;

    case kTone:
      sd_set_tone(value, &m_sd);
      break;

    case kSnappy:
      sd_set_snappy(value, &m_sd);
      break;

    case kDecay:
      sd_set_decay(value, &m_sd);
      break;

    default:
      assert(false);
      break;
  };
}

}  // namespace sd

namespace clap {

void Clap::initialize(double sample_rate, size_t /*max_block_size*/) {
  clap_init_parameters(&m_clap_parameters);
  clap_init(sample_rate, &m_clap);
}

void Clap::noteOn(int note_nr, double velocity) {
  if (in_range(note_nr, m_low_note, m_high_note)) {
    clap_set_gain(velocity, &m_clap_parameters);
    clap_note_on(&m_clap_parameters, &m_clap);
  }
}

void Clap::process(double *left, double *right, int number_of_frames) {
  clap_process(left, number_of_frames, &m_clap_parameters, &m_clap);
  memcpy(right, left, number_of_frames * sizeof(double));
}

void Clap::setParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kLowNote:
      m_low_note = pitch_value(value);
      break;

    case kHighNote:
      m_high_note = pitch_value(value);
      break;

    case kTone:
      clap_set_cutoff(value, &m_clap_parameters);
      break;

    case kReso:
      clap_set_resonance(value, &m_clap_parameters);
      break;

    case kDecay:
      clap_set_decay(value, &m_clap_parameters);
      break;

    case kEarlyDecay:
      clap_set_early_decay(value, &m_clap_parameters);
      break;

    case kEarlyDelay:
      clap_set_early_delay(value, &m_clap_parameters);
      break;

    default:
      assert(false);
      break;
  };
}

}  // namespace clap

namespace cymbal {

void Cymbal::initialize(double sample_rate, size_t /*max_block_size*/) {
  cymbal_init_parameters(&m_cymbal_parameters);
  cymbal_init(sample_rate, &m_cymbal);
}

void Cymbal::noteOn(int note_nr, double velocity) {
  int pitch = 0;
  if (in_range(note_nr, m_low_note, m_high_note, m_pitch, pitch)) {
    cymbal_set_gain(velocity, &m_cymbal_parameters);
    cymbal_set_frequency(m_pitch_to_frequency.frequency(pitch), &m_cymbal_parameters);
    cymbal_note_on(&m_cymbal_parameters, &m_cymbal);
  }
}

void Cymbal::process(double *left, double *right, int number_of_frames) {
  cymbal_process(left, number_of_frames, &m_cymbal_parameters, &m_cymbal);
  memcpy(right, left, number_of_frames * sizeof(double));
}

void Cymbal::setParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kLowNote:
      m_low_note = pitch_value(value);
      break;

    case kHighNote:
      m_high_note = pitch_value(value);
      break;

    case kPitch:
      m_pitch = pitch_value(value);
      break;

    case kTone:
      cymbal_set_cutoff(value, &m_cymbal_parameters);
      break;

    case kReso:
      cymbal_set_resonance(value, &m_cymbal_parameters);
      break;

    case kEarlyDecay:
      cymbal_set_early_decay(value, &m_cymbal_parameters);
      break;

    case kLateDecay:
      cymbal_set_late_decay(value, &m_cymbal_parameters);
      break;

    case kDecayThreshold:
      cymbal_set_decay_threshold(value, &m_cymbal_parameters);
      break;

    case kNoise:
      cymbal_set_noise(metal_noise_type(value), &m_cymbal_parameters);
    break;

    default:
      assert(false);
      break;
  };
}

}  // namespace cymbal

namespace hihat {

void Hihat::initialize(double sample_rate, size_t /*max_block_size*/) {
  hihat_init_parameters(&m_hihat_parameters);
  hihat_init(sample_rate, &m_hihat);
}

void Hihat::noteOn(int note_nr, double velocity) {
  if (note_nr == m_closed_note) {
    hihat_set_gain(velocity, &m_hihat_parameters);
    hihat_set_frequency(m_pitch_to_frequency.frequency(m_pitch), &m_hihat_parameters);
    hihat_closed_on(&m_hihat_parameters, &m_hihat);
  } else if (note_nr == m_open_note) {
      hihat_open_on(&m_hihat_parameters, &m_hihat);
  } else if (note_nr == m_pedal_note) {
    hihat_pedal_on(&m_hihat_parameters, &m_hihat);
  }
}

void Hihat::process(double *left, double *right, int number_of_frames) {
  hihat_process(left, number_of_frames, &m_hihat_parameters, &m_hihat);
  memcpy(right, left, number_of_frames * sizeof(double));
}

void Hihat::setParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kClosedNote:
      m_closed_note = pitch_value(value);
      break;

    case kOpenNote:
      m_open_note = pitch_value(value);
      break;

    case kPedalNote:
      m_pedal_note = pitch_value(value);
      break;

    case kPitch:
      m_pitch = pitch_value(value);
      break;

    case kNoise:
      hihat_set_noise(metal_noise_type(value), &m_hihat_parameters);
    break;

    case kTone:
      hihat_set_cutoff(value, &m_hihat_parameters);
      break;

    case kReso:
      hihat_set_resonance(value, &m_hihat_parameters);
      break;

    case kClosedDecay:
      hihat_set_closed_decay(value, &m_hihat_parameters);
      break;

    case kOpenDecay:
      hihat_set_open_decay(value, &m_hihat_parameters);
      break;

    case kPedalDecay:
      hihat_set_pedal_decay(value, &m_hihat_parameters);
      break;

    default:
      assert(false);
      break;
  };
}

}  // namespace hihat

namespace cb {

void Cb::initialize(double sample_rate, size_t /*max_block_size*/) {
  cb_init(sample_rate, &m_cb);
}

void Cb::noteOn(int note_nr, double velocity) {
  int pitch = 0;
  if (in_range(note_nr, m_low_note, m_high_note, m_pitch, pitch)) {
    cb_set_gain(velocity, &m_cb);
    cb_set_frequency(m_pitch_to_frequency.frequency(pitch), &m_cb);
    cb_note_on(&m_cb);
  }
}

void Cb::process(double *left, double *right, int number_of_frames) {
  cb_process(left, number_of_frames, &m_cb);
  memcpy(right, left, number_of_frames * sizeof(double));
}

void Cb::setParameter(int32_t parameter_id, double value) {
  switch (parameter_id) {
    case kLowNote:
      m_low_note = pitch_value(value);
      break;

    case kHighNote:
      m_high_note = pitch_value(value);
      break;

    case kPitch:
      m_pitch = pitch_value(value);
      break;

    case kDecay:
      cb_set_decay(value, &m_cb);
      break;

    default:
      assert(false);
      break;
  };
}

}  // namespace cb

DrumProcessor::DrumProcessor(const Steinberg::FUID& controller_uid,
                             std::unique_ptr<filurvst::State> state,
                             std::unique_ptr<module::ModuleIf> module)
: m_state(std::move(state)),
  m_module(std::move(module)) {
  setControllerClass(controller_uid);
}

Steinberg::tresult DrumProcessor::initialize(FUnknown* context) {
  Steinberg::tresult ret_val = AudioEffect::initialize(context);

  if (Steinberg::kResultTrue == ret_val) {
    addAudioOutput(STR16("output"), Steinberg::Vst::SpeakerArr::kStereo);
    addEventInput(STR16("event input"), 1);
  }

  return ret_val;
}

Steinberg::tresult DrumProcessor::setState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if (m_state) {
    ret_val = m_state->setState(state);

    if (m_is_active) {
      set_state_parameters(m_state.get(), m_module.get());
    }
  }

  return ret_val;
}

Steinberg::tresult DrumProcessor::getState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if (m_state) {
    ret_val = m_state->getState(state);
  }

  return ret_val;
}

Steinberg::tresult DrumProcessor::setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                                     Steinberg::int32 num_inputs,
                                                     Steinberg::Vst::SpeakerArrangement* outputs,
                                                     Steinberg::int32 num_outputs) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((0 == num_inputs) && (1 == num_outputs)
      && (Steinberg::Vst::SpeakerArr::kStereo == outputs[0])) {
    ret_val = AudioEffect::setBusArrangements(inputs, num_inputs, outputs, num_outputs);
  }

  return ret_val;
}

Steinberg::tresult DrumProcessor::canProcessSampleSize(Steinberg::int32 symbolic_sample_size) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((Steinberg::Vst::kSample32 == symbolic_sample_size)
      || (Steinberg::Vst::kSample64 == symbolic_sample_size)) {
    ret_val = Steinberg::kResultTrue;
  }

  return ret_val;
}

Steinberg::tresult DrumProcessor::setActive(Steinberg::TBool state) {
  m_is_active = state;
  if (state) {
    if (m_module) {
      m_module->initialize(processSetup.sampleRate, processSetup.maxSamplesPerBlock);
    }
    m_left_buffer.resize(processSetup.maxSamplesPerBlock);
    m_right_buffer.resize(processSetup.maxSamplesPerBlock);
    set_state_parameters(m_state.get(), m_module.get());
  }

  return AudioEffect::setActive(state);
}

Steinberg::tresult DrumProcessor::process(Steinberg::Vst::ProcessData& data) {
  Steinberg::tresult result = Steinberg::kResultTrue;

  handle_parameter_changes(data.inputParameterChanges, m_state.get(), m_module.get());

  if ((data.numOutputs < 1) || (data.outputs[0].numChannels < 2)) {
    return Steinberg::kResultTrue;
  }

  if (!m_module) {
    return Steinberg::kResultTrue;
  }

  Steinberg::int32 number_of_events = 0;
  Steinberg::Vst::IEventList* input_events = data.inputEvents;
  if (nullptr != input_events) {
    number_of_events = input_events->getEventCount();
  }

  assert(static_cast<size_t>(data.numSamples) <= m_left_buffer.size());
  assert(static_cast<size_t>(data.numSamples) <= m_right_buffer.size());
  double* out_left_buffer = m_left_buffer.data();
  double* out_right_buffer = m_right_buffer.data();
  memset(out_left_buffer, 0, data.numSamples * sizeof(double));
  memset(out_right_buffer, 0, data.numSamples * sizeof(double));

  bool silent = m_module->silent();

  Steinberg::int32 number_of_frames = data.numSamples;
  Steinberg::int32 frame_offset = 0;
  Steinberg::Vst::Event event;

  for (Steinberg::int32 i = 0; i < number_of_events; i++) {
    Steinberg::tresult handle_event = input_events->getEvent(i, event);

    if (Steinberg::kResultTrue == handle_event) {
      switch (event.type) {
        // supported event types
        case Steinberg::Vst::Event::kNoteOnEvent:
          break;

        // ignore all other event types
        default:
          handle_event = Steinberg::kResultFalse;
          break;
      }
    }

    if (Steinberg::kResultTrue == handle_event) {
      Steinberg::int32 number_of_sub_frames = event.sampleOffset - frame_offset;
      frame_offset = event.sampleOffset;

      if (number_of_sub_frames > 0) {
        m_module->process(out_left_buffer, out_right_buffer, number_of_sub_frames);
        out_left_buffer += number_of_sub_frames;
        out_right_buffer += number_of_sub_frames;
        number_of_frames -= number_of_sub_frames;
        silent &= m_module->silent();
      }

      if (Steinberg::Vst::Event::kNoteOnEvent == event.type) {
        m_module->noteOn(event.noteOn.pitch, event.noteOn.velocity);
      }
    }
  }

  if (number_of_frames > 0) {
    m_module->process(out_left_buffer, out_right_buffer, number_of_frames);
    silent &= m_module->silent();
  }

  if (Steinberg::Vst::kSample64 == data.symbolicSampleSize) {
    to_sample64(m_left_buffer.data(), m_right_buffer.data(), data);
  } else {
    to_sample32(m_left_buffer.data(), m_right_buffer.data(), data);
  }

  if (silent) {
    data.outputs[0].silenceFlags = 0x03;
  }

  return result;
}

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                                     filurvst::State* state,
                                     module::ModuleIf* module) {
  Steinberg::int32 parameter_count = 0;
  if ((nullptr != parameter_changes) && (nullptr != state)) {
    parameter_count = parameter_changes->getParameterCount();
  }

  for (Steinberg::int32 i = 0; i < parameter_count; i++) {
    Steinberg::tresult result = Steinberg::kResultTrue;
    Steinberg::Vst::IParamValueQueue* queue = parameter_changes->getParameterData(i);
    if (nullptr == queue) {
      result = Steinberg::kResultFalse;
    }

    if ((Steinberg::kResultTrue == result) && (0 == queue->getPointCount())) {
      result = Steinberg::kResultFalse;
    }

    Steinberg::Vst::ParamValue normalized_value;
    Steinberg::int32 sample_offset;
    if (Steinberg::kResultTrue == result) {
      result = queue->getPoint(queue->getPointCount() - 1, sample_offset, normalized_value);
    }

    if (Steinberg::kResultTrue == result) {
      state->setNormalized(queue->getParameterId(), normalized_value);
      double value = state->get(queue->getParameterId());
      set_parameter(queue->getParameterId(), value, module);
    }
  }

  if ((nullptr == parameter_changes) || (nullptr == state)) {
    return;
  }
}

static void set_parameter(Steinberg::Vst::ParamID parameter_id, double value, module::ModuleIf* module) {
  if (nullptr != module) {
    module->setParameter(parameter_id, value);
  }
}

static void set_state_parameters(filurvst::State* state, module::ModuleIf* module) {
  if (nullptr != state) {
    for (auto &parameter : state->parameters()) {
      set_parameter(parameter.first, parameter.second, module);
    }
  }
}

static void to_sample32(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data) {
  for (auto n = 0; n < data.numSamples; ++n) {
    data.outputs[0].channelBuffers32[0][n] = left_buffer[n];
    data.outputs[0].channelBuffers32[1][n] = right_buffer[n];
  }
}

static void to_sample64(const double* left_buffer, const double* right_buffer, const Steinberg::Vst::ProcessData& data) {
  memcpy(data.outputs[0].channelBuffers64[0], left_buffer, data.numSamples * sizeof(double));
  memcpy(data.outputs[0].channelBuffers64[1], right_buffer, data.numSamples * sizeof(double));
}

Steinberg::FUnknown* createBdProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new bd::Bd());
  bd::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(bd::cuid, std::move(state), std::move(module)));
  return processor.release();
}

Steinberg::FUnknown* createTomProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new tom::Tom());
  tom::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(tom::cuid, std::move(state), std::move(module)));
  return processor.release();
}

Steinberg::FUnknown* createKickProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new kick::Kick());
  kick::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(kick::cuid, std::move(state), std::move(module)));
  return processor.release();
}

Steinberg::FUnknown* createSdProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new sd::Sd());
  sd::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(sd::cuid, std::move(state), std::move(module)));
  return processor.release();
}

Steinberg::FUnknown* createClapProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new clap::Clap());
  clap::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(clap::cuid, std::move(state), std::move(module)));
  return processor.release();
}

Steinberg::FUnknown* createCymbalProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new cymbal::Cymbal());
  cymbal::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(cymbal::cuid, std::move(state), std::move(module)));
  return processor.release();
}

Steinberg::FUnknown* createHihatProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new hihat::Hihat());
  hihat::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(hihat::cuid, std::move(state), std::move(module)));
  return processor.release();
}

Steinberg::FUnknown* createCbProcessor(void*) {
  std::unique_ptr<filurvst::State> state(new filurvst::State);
  std::unique_ptr<module::ModuleIf> module(new cb::Cb());
  cb::init_state(*state);
  std::unique_ptr<Steinberg::Vst::IAudioProcessor> processor;
  processor.reset(new DrumProcessor(cb::cuid, std::move(state), std::move(module)));
  return processor.release();
}

}  // namespace drumelidrum
