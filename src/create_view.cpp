/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "controller.h"
#include "parameter_ids.h"
#include "module_types.h"

#include <filurvst/gui/uiviewcreator.h>

#include <vstgui/uidescription/uiviewfactory.h>
#include <vstgui/uidescription/uiattributes.h>

namespace drumelidrum {

static std::string get_module_template_name(const VSTGUI::IUIDescription* description, int pad_id);

class DrumModuleCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  DrumModuleCreator() { VSTGUI::UIViewFactory::registerViewCreator(*this); }
  VSTGUI::IdStringPtr getViewName() const override { return "DrumModule"; }
  VSTGUI::IdStringPtr getBaseViewName() const override { return nullptr; }
  VSTGUI::UTF8StringPtr getDisplayName() const override { return "Drum Module"; }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    static const std::string kAttrInstance = "instance";
    static const std::string view_name("DrumModuleView");
    VSTGUI::CView* module_view = nullptr;
    int instance;

    VSTGUI::CView* view = description->createView(view_name.c_str(), description->getController());

    // Create module view based on the module parameter for instance
    if (attributes.getIntegerAttribute(kAttrInstance, instance)) {
      std::string module_name = get_module_template_name(description, instance);
      module_view = description->createView(module_name.c_str(), description->getController());
    }

    if ((nullptr != view) &&
        (nullptr != view->asViewContainer()) &&
        (nullptr != module_view)) {
      view->asViewContainer()->addView(module_view);
    }

    if (nullptr != view) {
      const VSTGUI::IViewFactory* factory = description->getViewFactory();
      factory->applyAttributeValues(view, attributes, description);
    }

    return view;
  }
};

static std::string get_module_template_name(const VSTGUI::IUIDescription* description, int pad_id) {
  const static std::map<int, std::string> module_template_map = {
      { kModuleBd, "BdView" },
      { kModuleSd, "SdView" },
      { kModuleClosedHihat, "HihatView" },
      { kModulePedalHihat, "HihatView" },
      { kModuleOpenHihat, "HihatView" },
      { kModuleCymbal, "CymbalView" },
      { kModuleClap, "ClapView" },
      { kModuleRs, "SdView" },
      { kModuleClaves, "SdView" },
      { kModuleCowbell, "CbView" },
      { kModuleTom, "TomView" },
      { kModuleKick, "KickView" } };

  Steinberg::Vst::EditorView* editor_view = nullptr;
  Steinberg::Vst::EditController* edit_controller = nullptr;
  Steinberg::Vst::Parameter* parameter = nullptr;
  std::string module_name;

  if (nullptr != description) {
    editor_view = dynamic_cast<Steinberg::Vst::EditorView*>(description->getController());
  }

  if (nullptr != editor_view) {
    edit_controller = editor_view->getController();
  }

  if (nullptr != edit_controller) {
    int module_parameter_id = pad_base_id(pad_id) + kPadModule;
    parameter = edit_controller->getParameterObject(module_parameter_id);
  }

  if (nullptr != parameter) {
    int value = static_cast<int>(parameter->toPlain(parameter->getNormalized()));
    module_type_t module_type = static_cast<module_type_t>(value);
    if (module_template_map.count(module_type) > 0) {
      module_name = module_template_map.at(module_type);
    }
  }

  return module_name;
}

static DrumModuleCreator s_drum_module_creator;

}  // namespace drumelidrum
