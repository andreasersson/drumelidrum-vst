/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_PROCESSOR_H
#define DRUMELIDRUM_PROCESSOR_H

#include "parameter_state.h"
#include "modules/drumpad.h"

#include <public.sdk/source/vst/vstaudioeffect.h>

#include <memory>
#include <vector>

namespace drumelidrum {

class Indicator {
 public:
  Indicator(const Steinberg::Vst::ParamID &id = -1, double time = 150e-3, double silent_threshold = 0.0004);
  void init(Steinberg::Vst::SampleRate sample_rate);
  void setValue(double value);
  void process(Steinberg::Vst::IParameterChanges *parameterChanges, int num_samples);

 private:
  Steinberg::Vst::ParamID m_id;
  double m_silent_threshold;
  double m_time;
  int m_max_num_samples;
  int m_value;
  int m_previous_value;
};

class Processor : public Steinberg::Vst::AudioEffect, public module::ChokeIf {
 public:
  explicit Processor(const Steinberg::FUID& controller_id);

  Steinberg::tresult initialize(Steinberg::FUnknown* context) override;
  Steinberg::tresult setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                        Steinberg::int32 numIns,
                                        Steinberg::Vst::SpeakerArrangement* outputs,
                                        Steinberg::int32 numOuts) override;

  Steinberg::tresult setState(Steinberg::IBStream* state) override;
  Steinberg::tresult getState(Steinberg::IBStream* state) override;

  Steinberg::tresult canProcessSampleSize(Steinberg::int32 symbolicSampleSize) override;
  Steinberg::tresult setActive(Steinberg::TBool state) override;
  Steinberg::tresult process(Steinberg::Vst::ProcessData& data) override;

  virtual void choke(int id) override;

  static Steinberg::FUID cid;

 private:
  ParameterState m_state;

  std::vector<double> m_left_buffer;
  std::vector<double> m_right_buffer;

  std::vector<std::unique_ptr<module::DrumPad>> m_drumpads;
  std::vector<Indicator> m_drumpad_indicators;

  Steinberg::TBool m_is_active = {0};
};

}  // namespace drumelidrum

#endif // DRUMELIDRUM_PROCESSOR_H
