/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_DRUM_PARAMETERS_H
#define DRUMELIDRUM_DRUM_PARAMETERS_H

#include <filurvst/common/parameter_state.h>

#include <public.sdk/source/vst/vstparameters.h>

#include <string>

namespace drumelidrum {

namespace bd {
enum {
  kLowNote,
  kHighNote,
  kPitch,
  kTone,
  kDecay,
  kSweepDecay,
  kSweep,
  kSweepThreshold
};

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace bd

namespace tom {

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace tom

namespace kick {

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace kick

namespace sd {
enum {
  kLowNote,
  kHighNote,
  kPitch,
  kTone,
  kSnappy,
  kDecay
};

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace sd

namespace clap {
enum {
  kLowNote,
  kHighNote,
  kTone,
  kReso,
  kDecay,
  kEarlyDecay,
  kEarlyDelay
};

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace clap

namespace cymbal {
enum {
  kLowNote,
  kHighNote,
  kPitch,
  kTone,
  kReso,
  kEarlyDecay,
  kLateDecay,
  kDecayThreshold,
  kNoise
};

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace cymbal

namespace hihat {
enum {
  kClosedNote,
  kOpenNote,
  kPedalNote,
  kPitch,
  kNoise,
  kTone,
  kReso,
  kClosedDecay,
  kOpenDecay,
  kPedalDecay
};

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace hihat

namespace cb {
enum {
  kLowNote,
  kHighNote,
  kPitch,
  kDecay,
};

void init_state(filurvst::State& state);
void init_parameters(Steinberg::Vst::ParameterContainer& parameters);

} // namespace cb

} // namespace drumelidrum

#endif // DRUMELIDRUM_DRUM_PARAMETERS_H
