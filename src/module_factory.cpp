/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "module_factory.h"

#include "module_parameters.h"

#include <cassert>

namespace drumelidrum {

using drumpad_ptr = std::unique_ptr<module::DrumPad>;
using module_ptr = std::unique_ptr<module::ModuleIf>;

drumpad_ptr drumpad_module(filurvst::State &parameter_state, int id, module::ChokeIf* choke) {
  const module::ParameterIf::parameter_map_t pmap = drumpad_parameter_map(id);
  std::unique_ptr<module::DrumPadParameters> parameters(new module::DrumPadParameters(parameter_state, id, pmap));
  drumpad_ptr module(new module::DrumPad(std::move(parameters), choke));

  return module;
}

module_ptr bd_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = bd_parameter_map(pad_id);
  std::unique_ptr<module::BdParameters> parameters(new module::BdParameters(parameter_state, pmap));
  std::unique_ptr<module::Bd> module(new module::Bd(std::move(parameters)));

  return module;
}

module_ptr kick_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = kick_parameter_map(pad_id);
  std::unique_ptr<module::BdParameters> parameters(new module::BdParameters(parameter_state, pmap));
  std::unique_ptr<module::Kick> module(new module::Kick(std::move(parameters)));

  return module;
}

module_ptr tom_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = tom_parameter_map(pad_id);
  std::unique_ptr<module::TomParameters> parameters(new module::TomParameters(parameter_state, pmap));
  std::unique_ptr<module::Tom> module(new module::Tom(std::move(parameters)));

  return module;
}

module_ptr sd_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = sd_parameter_map(pad_id);
  std::unique_ptr<module::SdParameters> parameters(new module::SdParameters(parameter_state, pmap));
  std::unique_ptr<module::Sd> module(new module::Sd(std::move(parameters)));

  return module;
}

module_ptr rs_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = sd_parameter_map(pad_id);
  std::unique_ptr<module::RsParameters> parameters(new module::RsParameters(parameter_state, pmap));
  std::unique_ptr<module::Sd> module(new module::Sd(std::move(parameters)));

  return module;
}

module_ptr claves_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = sd_parameter_map(pad_id);
  std::unique_ptr<module::ClavesParameters> parameters(new module::ClavesParameters(parameter_state, pmap));
  std::unique_ptr<module::Sd> module(new module::Sd(std::move(parameters)));

  return module;
}

module_ptr clap_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = clap_parameter_map(pad_id);
  std::unique_ptr<module::ClapParameters> parameters(new module::ClapParameters(parameter_state, pmap));
  std::unique_ptr<module::Clap> module(new module::Clap(std::move(parameters)));

  return module;
}

module_ptr ch_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = closed_hihat_parameter_map(pad_id);
  std::unique_ptr<module::HihatParameters> parameters(new module::HihatParameters(parameter_state, pmap));
  std::unique_ptr<module::ClosedHihat> module(new module::ClosedHihat(std::move(parameters)));

  return module;
}

module_ptr ph_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = pedal_hihat_parameter_map(pad_id);
  std::unique_ptr<module::HihatParameters> parameters(new module::HihatParameters(parameter_state, pmap));
  std::unique_ptr<module::PedalHihat> module(new module::PedalHihat(std::move(parameters)));

  return module;
}

module_ptr oh_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = open_hihat_parameter_map(pad_id);
  std::unique_ptr<module::HihatParameters> parameters(new module::HihatParameters(parameter_state, pmap));
  std::unique_ptr<module::OpenHihat> module(new module::OpenHihat(std::move(parameters)));

  return module;
}

module_ptr cb_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = cb_parameter_map(pad_id);
  std::unique_ptr<module::CbParameters> parameters(new module::CbParameters(parameter_state, pmap));
  std::unique_ptr<module::Cb> module(new module::Cb(std::move(parameters)));

  return module;
}

module_ptr cymbal_module(filurvst::State &parameter_state, int pad_id) {
  const module::ParameterIf::parameter_map_t pmap = cymbal_parameter_map(pad_id);
  std::unique_ptr<module::CymbalParameters> parameters(new module::CymbalParameters(parameter_state, pmap));
  std::unique_ptr<module::Cymbal> module(new module::Cymbal(std::move(parameters)));

  return module;
}

module_ptr module_factory(filurvst::State &parameter_state, int pad_id, module_type_t type) {
  module_ptr module;
  switch (type) {
    case kModuleBd:
      module = bd_module(parameter_state, pad_id);
      break;

    case kModuleSd:
      module = sd_module(parameter_state, pad_id);
      break;

    case kModuleClosedHihat:
      module = ch_module(parameter_state, pad_id);
      break;

    case kModulePedalHihat:
      module = ph_module(parameter_state, pad_id);
      break;

    case kModuleOpenHihat:
      module = oh_module(parameter_state, pad_id);
      break;

    case kModuleCymbal:
      module = cymbal_module(parameter_state, pad_id);
      break;

    case kModuleClap:
      module = clap_module(parameter_state, pad_id);
      break;

    case kModuleRs:
      module = rs_module(parameter_state, pad_id);
      break;

    case kModuleClaves:
      module = claves_module(parameter_state, pad_id);
      break;

    case kModuleCowbell:
      module = cb_module(parameter_state, pad_id);
      break;

    case kModuleTom:
      module = tom_module(parameter_state, pad_id);
      break;

    case kModuleKick:
      module = kick_module(parameter_state, pad_id);
      break;

    default:
      assert(false);
      break;
  };

  return module;
}

}  // namespace drumelidrum
