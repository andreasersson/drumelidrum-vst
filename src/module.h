/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRUMELIDRUM_MODULE_H
#define DRUMELIDRUM_MODULE_H

#include <filurvst/common/parameter_state.h>

#include <vector>

namespace module {

class ParameterIf {
 public:
  typedef std::map<int32_t, int32_t> parameter_map_t;

  ParameterIf(const parameter_map_t &parameter_map)
      : m_parameter_ids(parameter_map) {}
  virtual ~ParameterIf() {}

  virtual void initialize(double sample_rate) = 0;
  virtual void handleParameter(int32_t parameter_id, double value) = 0;

  virtual void set(int32_t parameter_id, double value) {
    if (m_parameter_ids.count(parameter_id) > 0) {
      handleParameter(m_parameter_ids.at(parameter_id), value);
    }
  }

 protected:
  parameter_map_t m_parameter_ids;
};

class ModuleIf {
 public:
  virtual ~ModuleIf() {}

  virtual void initialize(double sample_rate, size_t max_block_size) = 0;
  virtual void noteOn(int note_nr, double velocity) = 0;
  virtual void noteOff(int note_nr) = 0;
  virtual void choke() = 0;
  virtual void process(double *left, double *right, int number_of_frames) = 0;
  virtual bool silent() = 0;
  virtual double level() const = 0;
  virtual void setParameter(int32_t parameter_id, double value) = 0;
  virtual ParameterIf* getParameters() = 0;
};

class ChokeIf {
 public:
  virtual ~ChokeIf() {}
  virtual void choke(int id) = 0;
};

}  // namespace module

#endif // DRUMELIDRUM_MODULE_H
