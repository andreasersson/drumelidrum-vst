/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "parameter_ids.h"
#include "parameter_state.h"
#include "module_parameters.h"

template <class T> T test_cast(double value) {
  return static_cast<T>(value);
}

template<> metal_noise_type_t test_cast(double value) {
  int iv = static_cast<int>(value);
  return static_cast<metal_noise_type_t>(iv);
}

template<class T> bool test_set_parameter(drumelidrum::ParameterState& state,
                                          int pad_id,
                                          Steinberg::Vst::ParamID parameter_id,
                                          module::ParameterIf& parameters,
                                          T& value) {
  Steinberg::Vst::ParamID pid = drumelidrum::module_base_id(pad_id) + parameter_id;
  state.setNormalized(pid, 0.0);
  T min_value = test_cast<T>(state.get(pid));
  parameters.set(pid, min_value);
  T pre_value = value;

  state.setNormalized(pid, 1.0);
  T max_value = test_cast<T>(state.get(pid));
  parameters.set(pid, max_value);

  bool ret_val = (pre_value != value);

  return ret_val;
}

TEST(DrumelidrumParameters, bd) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::bd_parameter_map(pad_id);
  module::BdParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kPitch, parameters, parameters.bd.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kTone, parameters, parameters.bd.tone));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kDecay, parameters, parameters.bd.decay));
}

TEST(DrumelidrumParameters, kick) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::kick_parameter_map(pad_id);
  module::BdParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kPitch, parameters, parameters.bd.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kTone, parameters, parameters.bd.tone));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kDecay, parameters, parameters.bd.decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kSweepDecay, parameters, parameters.bd.sweep_decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kSweep, parameters, parameters.bd.sweep));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kSweepThreshold, parameters, parameters.bd.sweep_threshold));
}

TEST(DrumelidrumParameters, tom) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::tom_parameter_map(pad_id);
  module::BdParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kPitch, parameters, parameters.bd.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kTone, parameters, parameters.bd.tone));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kDecay, parameters, parameters.bd.decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kSweepDecay, parameters, parameters.bd.sweep_decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::BdParameters::kSweepThreshold, parameters, parameters.bd.sweep_threshold));
}

TEST(DrumelidrumParameters, cb) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::cb_parameter_map(pad_id);
  module::CbParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CbParameters::kPitch, parameters, parameters.cb.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CbParameters::kDecay, parameters, parameters.cb.decay));
}

TEST(DrumelidrumParameters, clap) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::clap_parameter_map(pad_id);
  module::ClapParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::ClapParameters::kTone, parameters, parameters.clap.cutoff));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::ClapParameters::kReso, parameters, parameters.clap.resonance));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::ClapParameters::kDecay, parameters, parameters.clap.decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::ClapParameters::kEarlyDecay, parameters, parameters.clap.early_decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::ClapParameters::kEarlyDelay, parameters, parameters.clap.early_delay));
}

TEST(DrumelidrumParameters, cymbal) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::cymbal_parameter_map(pad_id);
  module::CymbalParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CymbalParameters::kPitch, parameters, parameters.cymbal.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CymbalParameters::kTone, parameters, parameters.cymbal.cutoff));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CymbalParameters::kReso, parameters, parameters.cymbal.resonance));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CymbalParameters::kEarlyDecay, parameters, parameters.cymbal.early_decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CymbalParameters::kLateDecay, parameters, parameters.cymbal.late_decay));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CymbalParameters::kDecayThreshold, parameters, parameters.cymbal.decay_threshold));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::CymbalParameters::kNoise, parameters, parameters.cymbal.noise));
}

TEST(DrumelidrumParameters, closed_hihat) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::closed_hihat_parameter_map(pad_id);
  module::HihatParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kPitch, parameters, parameters.hihat.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kNoise, parameters, parameters.hihat.noise));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kTone, parameters, parameters.hihat.cutoff));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kReso, parameters, parameters.hihat.resonance));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kClosedDecay, parameters, parameters.hihat.closed_decay));
}

TEST(DrumelidrumParameters, open_hihat) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::open_hihat_parameter_map(pad_id);
  module::HihatParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kPitch, parameters, parameters.hihat.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kNoise, parameters, parameters.hihat.noise));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kTone, parameters, parameters.hihat.cutoff));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kReso, parameters, parameters.hihat.resonance));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kClosedDecay, parameters, parameters.hihat.open_decay));
}

TEST(DrumelidrumParameters, pedal_hihat) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::pedal_hihat_parameter_map(pad_id);
  module::HihatParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kPitch, parameters, parameters.hihat.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kNoise, parameters, parameters.hihat.noise));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kTone, parameters, parameters.hihat.cutoff));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kReso, parameters, parameters.hihat.resonance));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::HihatParameters::kClosedDecay, parameters, parameters.hihat.pedal_decay));
}

TEST(DrumelidrumParameters, rs) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::sd_parameter_map(pad_id);
  module::RsParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::RsParameters::kPitch, parameters, parameters.sd.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::RsParameters::kTone, parameters, parameters.sd.tone));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::RsParameters::kSnappy, parameters, parameters.sd.snappy));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::RsParameters::kDecay, parameters, parameters.sd.decay));
}

TEST(DrumelidrumParameters, sd) {
  drumelidrum::ParameterState state;
  int pad_id = 0;
  const module::ParameterIf::parameter_map_t parameter_map = drumelidrum::sd_parameter_map(pad_id);
  module::SdParameters parameters(state, parameter_map);

  EXPECT_TRUE(test_set_parameter(state, pad_id, module::SdParameters::kPitch, parameters, parameters.sd.frequency));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::SdParameters::kTone, parameters, parameters.sd.tone));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::SdParameters::kSnappy, parameters, parameters.sd.snappy));
  EXPECT_TRUE(test_set_parameter(state, pad_id, module::SdParameters::kDecay, parameters, parameters.sd.decay));
}
