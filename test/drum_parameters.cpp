/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "drum_processor.h"
#include "module.h"
#include "drum_parameters.h"

#include <filurvst/common/parameter_state.h>

template <class T> T drum_cast(double value) {
  return static_cast<T>(value);
}

template<> metal_noise_type_t drum_cast(double value) {
  return metal_noise_type(value);
}

template<class T> bool test_set_parameter(filurvst::State& state,
                                          Steinberg::Vst::ParamID parameter_id,
                                          module::ModuleIf& module,
                                          const T& value) {
  state.setNormalized(parameter_id, 0.0);
  T min_value = drum_cast<T>(state.get(parameter_id));
  module.setParameter(parameter_id, min_value);
  T pre_value = value;

  state.setNormalized(parameter_id, 1.0);
  T max_value = drum_cast<T>(state.get(parameter_id));
  module.setParameter(parameter_id, max_value);

  bool ret_val = (pre_value != value);

  return ret_val;
}

class TestBd : public drumelidrum::bd::Bd {
 public:
  const bd_parameters_t& parameters() { return m_bd_parameters; }
  const int& pitch() { return m_pitch; }
  const int& low_note() { return m_low_note; }
  const int& high_note() { return m_high_note; }
};

class TestTom : public drumelidrum::tom::Tom {
 public:
  const bd_parameters_t& parameters() { return m_bd_parameters; }
  const int& pitch() { return m_pitch; }
  const int& low_note() { return m_low_note; }
  const int& high_note() { return m_high_note; }
};

class TestKick : public drumelidrum::kick::Kick {
 public:
  const bd_parameters_t& parameters() { return m_bd_parameters; }
  const int& pitch() { return m_pitch; }
  const int& low_note() { return m_low_note; }
  const int& high_note() { return m_high_note; }
};

class TestSd : public drumelidrum::sd::Sd {
 public:
  const sd_t& parameters() { return m_sd; }
  const int& pitch() { return m_pitch; }
  const int& low_note() { return m_low_note; }
  const int& high_note() { return m_high_note; }
};

class TestClap : public drumelidrum::clap::Clap {
 public:
  const clap_parameters_t& parameters() { return m_clap_parameters; }
  const int& low_note() { return m_low_note; }
  const int& high_note() { return m_high_note; }
};

class TestCymbal : public drumelidrum::cymbal::Cymbal {
 public:
  const cymbal_parameters_t& parameters() { return m_cymbal_parameters; }
  const int& pitch() { return m_pitch; }
  const int& low_note() { return m_low_note; }
  const int& high_note() { return m_high_note; }
};

class TestHihat : public drumelidrum::hihat::Hihat {
 public:
  const hihat_parameters_t& parameters() { return m_hihat_parameters; }
  const int& pitch() { return m_pitch; }
  const int& closed_note() { return m_closed_note; }
  const int& open_note() { return m_open_note; }
  const int& pedal_note() { return m_pedal_note; }
};

class TestCb : public drumelidrum::cb::Cb {
 public:
  const cb_t& parameters() { return m_cb; }
  const int& pitch() { return m_pitch; }
  const int& low_note() { return m_low_note; }
  const int& high_note() { return m_high_note; }
};

TEST(DrumParameters, BbSetParameters) {
  filurvst::State state;
  drumelidrum::bd::init_state(state);
  TestBd module;
  const bd_parameters_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kLowNote, module, module.low_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kHighNote, module, module.high_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kPitch, module, module.pitch()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kTone, module, parameters.tone));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kDecay, module, parameters.decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweepDecay, module, parameters.sweep_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweep, module, parameters.sweep));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweepThreshold, module, parameters.sweep_threshold));
}

TEST(DrumParameters, CbSetParameters) {
  filurvst::State state;
  drumelidrum::cb::init_state(state);
  TestCb module;
  const cb_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cb::kLowNote, module, module.low_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cb::kHighNote, module, module.high_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cb::kPitch, module, module.pitch()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cb::kDecay, module, parameters.decay));
}

TEST(DrumParameters, ClapSetParameters) {
  filurvst::State state;
  drumelidrum::clap::init_state(state);
  TestClap module;
  const clap_parameters_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::clap::kLowNote, module, module.low_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::clap::kHighNote, module, module.high_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::clap::kTone, module, parameters.cutoff));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::clap::kReso, module, parameters.resonance));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::clap::kDecay, module, parameters.decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::clap::kEarlyDecay, module, parameters.early_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::clap::kEarlyDelay, module, parameters.early_delay));
}

TEST(DrumParameters, CymbalSetParameters) {
  filurvst::State state;
  drumelidrum::cymbal::init_state(state);
  TestCymbal module;
  const cymbal_parameters_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kLowNote, module, module.low_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kHighNote, module, module.high_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kPitch, module, module.pitch()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kTone, module, parameters.cutoff));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kReso, module, parameters.resonance));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kEarlyDecay, module, parameters.early_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kLateDecay, module, parameters.late_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kDecayThreshold, module, parameters.decay_threshold));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::cymbal::kNoise, module, parameters.noise));
}

TEST(DrumParameters, HihatSetParameters) {
  filurvst::State state;
  drumelidrum::hihat::init_state(state);
  TestHihat module;
  const hihat_parameters_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kClosedNote, module, module.closed_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kOpenNote, module, module.open_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kPedalNote, module, module.pedal_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kPitch, module, module.pitch()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kNoise, module, parameters.noise));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kTone, module, parameters.cutoff));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kReso, module, parameters.resonance));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kClosedDecay, module, parameters.closed_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kOpenDecay, module, parameters.open_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::hihat::kPedalDecay, module, parameters.pedal_decay));
}

TEST(DrumParameters, KickSetParameters) {
  filurvst::State state;
  drumelidrum::kick::init_state(state);
  TestKick module;
  const bd_parameters_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kLowNote, module, module.low_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kHighNote, module, module.high_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kPitch, module, module.pitch()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kTone, module, parameters.tone));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kDecay, module, parameters.decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweepDecay, module, parameters.sweep_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweep, module, parameters.sweep));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweepThreshold, module, parameters.sweep_threshold));
}

TEST(DrumParameters, SdSetParameters) {
  filurvst::State state;
  drumelidrum::sd::init_state(state);
  TestSd module;
  const sd_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::sd::kLowNote, module, module.low_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::sd::kHighNote, module, module.high_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::sd::kPitch, module, module.pitch()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::sd::kTone, module, parameters.tone));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::sd::kSnappy, module, parameters.snappy));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::sd::kDecay, module, parameters.decay));
}

TEST(DrumParameters, TomSetParameters) {
  filurvst::State state;
  drumelidrum::tom::init_state(state);
  TestTom module;
  const bd_parameters_t& parameters = module.parameters();

  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kLowNote, module, module.low_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kHighNote, module, module.high_note()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kPitch, module, module.pitch()));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kTone, module, parameters.tone));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kDecay, module, parameters.decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweepDecay, module, parameters.sweep_decay));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweep, module, parameters.sweep));
  EXPECT_TRUE(test_set_parameter(state, drumelidrum::bd::kSweepThreshold, module, parameters.sweep_threshold));
}
