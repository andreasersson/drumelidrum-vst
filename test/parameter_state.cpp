/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "parameter_state.h"

#include <public.sdk/source/common/memorystream.h>

#include <random>

#define STATE_EXPECT_EQ(state1, state2) \
  for (auto& parameter : state1.parameters()) {\
    filurvst::parameter_info_t info;\
    ASSERT_TRUE(state1.info(parameter.first, info));\
    if (!info.runtime) {\
      EXPECT_EQ(state1.get(parameter.first), state2.get(parameter.first));\
    }\
  }

static void test_setup(filurvst::State& state1,
                       filurvst::State& state2,
                       std::uniform_real_distribution<double> distribution) {
  std::default_random_engine generator;
  Steinberg::MemoryStream stream;
  for (auto& parameter : state1.parameters()) {
    state1.setNormalized(parameter.first, distribution(generator));
  }

  state1.getState(&stream);
  stream.seek(0, Steinberg::IBStream::kIBSeekSet, nullptr);
  state2.setState(&stream);
}

static void test_setup_min(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(0.0, 0.0);
  test_setup(state1, state2, distribution);
}

static void test_setup_max(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(1.0, 1.0);
  test_setup(state1, state2, distribution);
}

static void test_setup_rnd(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(0.0, 1.0);
  test_setup(state1, state2, distribution);
}

TEST(DrumelidrumParameterState, GetSetState) {
  drumelidrum::ParameterState state1;
  drumelidrum::ParameterState state2;

  test_setup_min(state1, state2);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2);
  STATE_EXPECT_EQ(state1, state2)
}
