/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of drumelidrum.
 *
 * drumelidrum is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * drumelidrum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with drumelidrum.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "drum_parameters.h"

#include <filurvst/common/parameter_state.h>

#include <public.sdk/source/common/memorystream.h>

#include <functional>
#include <random>

#define STATE_EXPECT_EQ(state1, state2) \
  for (auto& parameter : state1.parameters()) {\
    filurvst::parameter_info_t info;\
    ASSERT_TRUE(state1.info(parameter.first, info));\
    if (!info.runtime) {\
      EXPECT_EQ(state1.get(parameter.first), state2.get(parameter.first));\
    }\
  }

static void test_setup(filurvst::State& state1,
                       filurvst::State& state2,
                       std::function<void(filurvst::State& state)> init,
                       std::uniform_real_distribution<double> distribution) {
  std::default_random_engine generator;
  init(state1);
  init(state2);
  Steinberg::MemoryStream stream;
  for (auto& parameter : state1.parameters()) {
    state1.setNormalized(parameter.first, distribution(generator));
  }

  state1.getState(&stream);
  stream.seek(0, Steinberg::IBStream::kIBSeekSet, nullptr);
  state2.setState(&stream);
}

static void test_setup_min(filurvst::State& state1,
                       filurvst::State& state2,
                       std::function<void(filurvst::State& state)> init) {
  std::uniform_real_distribution<double> distribution(0.0, 0.0);
  test_setup(state1, state2, init, distribution);
}

static void test_setup_max(filurvst::State& state1,
                       filurvst::State& state2,
                       std::function<void(filurvst::State& state)> init) {
  std::uniform_real_distribution<double> distribution(1.0, 1.0);
  test_setup(state1, state2, init, distribution);
}

static void test_setup_rnd(filurvst::State& state1,
                       filurvst::State& state2,
                       std::function<void(filurvst::State& state)> init) {
  std::uniform_real_distribution<double> distribution(0.0, 1.0);
  test_setup(state1, state2, init, distribution);
}

TEST(DrumParameterState, BdGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::bd::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::bd::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::bd::init_state);
  STATE_EXPECT_EQ(state1, state2)
}


TEST(DrumParameterState, CbGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::cb::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::cb::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::cb::init_state);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(DrumParameterState, ClapGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::clap::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::clap::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::clap::init_state);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(DrumParameterState, CymbalGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::cymbal::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::cymbal::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::cymbal::init_state);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(DrumParameterState, HihatGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::hihat::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::hihat::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::hihat::init_state);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(DrumParameterState, KickGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::kick::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::kick::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::kick::init_state);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(DrumParameterState, SdGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::sd::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::sd::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::sd::init_state);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(DrumParameterState, TomGetSetState) {
  filurvst::State state1;
  filurvst::State state2;

  test_setup_min(state1, state2, drumelidrum::tom::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2, drumelidrum::tom::init_state);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2, drumelidrum::tom::init_state);
  STATE_EXPECT_EQ(state1, state2)
}
